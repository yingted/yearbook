<?PHP
require_once('config.php');
require_once('year_lib.php');
require_once('people.php');

// Initialise variables from form
foreach($_REQUEST as $varname => $varval) {
  $_REQUEST[$varname]=stripslashes($_REQUEST[$varname]);
}

if(isset($_SESSION['id']) && !user_can('auth', 'long'))
{
  if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
  }
}

print start_html("Mathcamp 2012! Yearbook Homepage");
?>
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" type="text/css" href="ui-style.css">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
  <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js" type="text/javascript"></script>
</head>
<body>
<h1>Mathcamp 2012! Yearbook Homepage</h1>

<table>
<tr>

<td width="500" valign="top" class="highlight">

<h2>Edit Your Entry</h2>

<p>To edit your entry, just type in your name into the box and click
"OK".</p>

<form action="edit.php"
      enctype="multipart/form-data"
      method="post">
<center>

<script>
  (function( $ ) {
    $.widget( "ui.combobox", {
      _create: function() {
	  var input,
	    self = this,
	    select = this.element.hide(),
	    selected = select.children( ":selected" ),
	    value = selected.val() ? selected.text() : "",
	    wrapper = this.wrapper = $( "<span>" )
	    .addClass( "ui-combobox" )
	    .insertAfter( select );

	  input = $( "<input>" )
	    .appendTo( wrapper )
	    .val( value )
	    .addClass( "ui-state-default ui-combobox-input" )
	    .autocomplete({
	      delay: 0,
		  minLength: 0,
		  source: function( request, response ) {
		  var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
		  response( select.children( "option" ).map(function() {
			var text = $( this ).text();
			if ( this.value && ( !request.term || matcher.test(text) ) )
			  return {
			  label: text.replace(
					      new RegExp(
							 "(?![^&;]+;)(?!<[^<>]*)(" +
							 $.ui.autocomplete.escapeRegex(request.term) +
							 ")(?![^<>]*>)(?![^&;]+;)", "gi"
							 ), "<strong>$1</strong>" ),
			      value: text,
			      option: this
			      };
		      }) );
		},
		  select: function( event, ui ) {
		  ui.item.option.selected = true;
		  self._trigger( "selected", event, {
		    item: ui.item.option
			});
		},
		  change: function( event, ui ) {
		  if ( !ui.item ) {
		    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
		      valid = false;
		    select.children( "option" ).each(function() {
			if ( $( this ).text().match( matcher ) ) {
			  this.selected = valid = true;
			  return false;
			}
		      });
		    if ( !valid ) {
		      // remove invalid value, as it didn't match anything
		      $( this ).val( "" );
		      select.val( "" );
		      input.data( "autocomplete" ).term = "";
		      return false;
		    }
		  }
		}
	      })
	    .addClass( "ui-widget ui-widget-content ui-corner-left" );

	  input.data( "autocomplete" )._renderItem = function( ul, item ) {
	    return $( "<li></li>" )
	    .data( "item.autocomplete", item )
	    .append( "<a>" + item.label + "</a>" )
	    .appendTo( ul );
	  };

	  $( "<a>" )
	    .attr( "tabIndex", -1 )
	    .attr( "title", "Show All Items" )
	    .appendTo( wrapper )
	    .button({
	      icons: {
		primary: "ui-icon-triangle-1-s"
		    },
		  text: false
		  })
	    .removeClass( "ui-corner-all" )
	    .addClass( "ui-corner-right ui-combobox-toggle" )
	    .click(function() {
		// close if already visible
		if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
		  input.autocomplete( "close" );
		  return;
		}

		// work around a bug (likely same cause as #5265)
		$( this ).blur();

		// pass empty string as value to search for, displaying all results
		input.autocomplete( "search", "" );
		input.focus();
	      });
	},

	  destroy: function() {
	  this.wrapper.remove();
	  this.element.show();
	  $.Widget.prototype.destroy.call( this );
	}
      });
  })( jQuery );

$(function() {
    $( "#combobox" ).combobox();
    $( "#toggle" ).click(function() {
	$( "#combobox" ).toggle();
      });
  });
</script>


<div class="ui-widget">
  <label>Name: </label>
  <select id="combobox" name="file" size="15">
<?PHP
foreach ($people as $file => $person)
{
  print("    <option value=\"$file\">$person</option>\n");
}
?>
</select>
</div>

</center>

</br></br></br>
<?PHP
if (isset($_SESSION['id']) and user_can('auth')) {
  print <<<EOT
<p><b>
  Remember to <a href="logout.php">logout</a> or exit your browser
  (Safari, Firefox, IE, whatever) when you are done, as you have used
  the yearbook staff password!!
</b></p>

EOT;
} elseif ($usepasswords) {
  print <<<EOT
<p>Enter password to edit entry: <input type="password" name="pass" />
</p>

EOT;
} else {
  print <<<EOT
<p>Enter a password iff needed (after deadline, locked entry, or
  other special circumstance): <input type="password" name="pass" />
</p>

EOT;
}
?>

<p>
<input type="hidden" name="_charset_" />
<input type="submit" name="action" value="OK" />
</p>
</form>

</td>
<!-- ------------------------------------- -->
<td width="500" valign="top">

<h2>Yearbook Staff Corner</h2>
<p>If you are on the yearbook staff, you can visit the
<a href="admin/">admin section</a>.</p>

<h2>Submit Other Content</h2>

<ul>

<li>You can submit quotes online <a href="quotes.php">here</a>.
(You can still submit them on paper in the box at the notice board
too.)</li>

<li>Mathcamp staff, if you would like to submit a book recommendation,
click <a href="recommend.php">here</a>.</li>

<li>Click <a href="comments.php">here</a> to send a comment to the
Yearbook staff. </li>

</ul>

<h2>Using LaTeX</h2>

<p>Click <a href="latexHelp.html">here</a> for some information on how
you can best format your entry for the yearbook, even if you already know
how to use LaTeX.</p>

<hr>

<p>If you have any technical problems with the site (LaTeX code is disappearing, your personal page isn't being saved, etc), submit a comment, or talk to <?php echo $techiename ?>.</p>

</td>

</tr>
</table>

</body>
</html>
