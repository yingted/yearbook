<?php
require('../config.php');
require('year_lib.php');

session_start();

if (isset($_REQUEST['pass'])) {
    $user = $_REQUEST['user'];
    $pass = $_REQUEST['pass'];
    if ($pass === $passwords[$user]) {
        $_SESSION['id'] = $user;
        $_SESSION['idtype'] = $user;
    }
}

$loggedin = get_user();
?>
<html>
<head>
<title>Yearbook admin</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php
if($loggedin)
{
    ?>
<h1>Yearbook Admin Page</h1>

<p>You currently don't have many choices</p>

<dl>
<?php
if(user_can('quote', 'list'))
{
    print "<dt><a href='quotes.php'>Quotes editing</a></dt>
    <dd>Censor, categorize, and edit quotes</dd>\n";
}
if(user_can('rec', 'list'))
{
    print "<dt><a href='recommend.php'>Recommendation editing</a></dt>
    <dd>Censor, categorize, and edit recommendations</dd>\n";
}
if(user_can('galley', 'list'))
{
    print "<dt><a href='galleys.php'>View galleys</a></dt>
    <dd>View the current progress on different yearbook sections</dd>\n";
}
if(user_can('photo', 'edit'))
{
    print "<dt><a href='../photo'>Photo Management</a></dt>
    <dd>Manage camper and staff individual photos</dd>\n";
}
print "<dt><a href='suggestions.php'>Comments</a></dt>
<dd>View a list of submitted comments.</dd>\n";
print "<dt><a href='../'>Main page</a></dt>
<dd>Return to the main page to edit an entry</dd>\n";
print "<dt><a href='../logout'>Logout</a></dt>
<dd>Don't stay logged in forever</dd>\n";
?>
</dl>
    <?php
} else {
    ?>
    <h1>Not logged in</h1>
    <p>You are not logged in. If you are on the yearbook team, enter the staff
    password to log in. Otherwise, you can click <a href="../">here<a> to return
    to the main page.</p>

    <form method="post" action=".">
    <p><label>User: <input type="text" name="user" /></label></p>
    <p><label>Password: <input type="password" name="pass" /></label></p>
    <p><input type="submit" name="submit" value='Enter'/></p>
    </form>
    <?php
}
?>
</body>
</html>
