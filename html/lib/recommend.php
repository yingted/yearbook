<?php
require_once(dirname(__FILE__) . '/../config.php');
require_once('xml-lib.php');
require_once('year_lib.php');

class Recommendation extends DataFileManager
{
    var $progname = 'rec';
    var $name = 'recommendation';
    function __construct()
    {
        parent::__construct();
    }

    function describe_structure()
    {
        return array(
            array('@ordinal', 'Ordinal', 'text'),
            array('cat', 'cat',),
            array('author', ),
        );
    }

    function get_form($id, $admin = false)
    {
        // General setup
        $ret = '';
        $item = $this->get_item($id);
        //if($id==0) $id = $this->incr_curid();
        $xpath = $this->xpath;

        // Stuff
        ob_start();
        $title = $this->get_cv("title", $item);
        tab_item("Title", 'title', $title);
        $author = $this->get_cv("author", $item);
        tab_item("Author", 'author', $author);
        $comments = $this->get_cv("comments", $item);
        l_tab_item("Comments", 'comments', $comments);
        $ret .= ob_get_contents();
        ob_end_clean();

        $ret .= $this->form_append($item, $id, $admin);
        return $ret;
    }

    function create_new_node($id)
    {
        $element = $this->doc->createElement($this->progname);
        if($id==0) $id = $this->incr_curid();
        $title = clean_space(input_unescape_slashes($_REQUEST["title"]));
        $author = clean_space(input_unescape_slashes($_REQUEST["author"]));
        $comments = clean_space(input_unescape_slashes($_REQUEST["comments"]));
        $element->appendChild($this->doc->createElement('title', $title));
        $element->appendChild($this->doc->createTextNode("\n"));
        $element->appendChild($this->doc->createElement('author', $author));
        $element->appendChild($this->doc->createTextNode("\n"));
        $element->appendChild($this->doc->createElement('comments', $comments));
        $element->appendChild($this->doc->createTextNode("\n"));
        $this->create_new_node_std($element, $id);
        return $element;
    }
}
?>
