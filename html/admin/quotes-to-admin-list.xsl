<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:transform version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >
    <xsl:output omit-xml-declaration="yes" method="html" indent="yes" />

    <xsl:template match='/' name='process-all'>
        <xsl:param name='reqstatus' />
        <xsl:param name='category' />

        <h2>Items</h2>
        <p><a href='?action=edit&amp;id=0'>Submit New</a></p>
        <table class="item-table quotes-table">
        <thead>
            <tr>
                <th>Submitter<br />Status<br />Category<br /></th>
                <th>Quote</th>
            </tr>
        </thead><tbody>
        <xsl:for-each select="/*/items/*[@idnum!=0]">
            <xsl:call-template name='process-item' />
        </xsl:for-each>
        </tbody>
        </table>
        <p><a href='?action=edit&amp;id=0'>Submit New</a></p>

        <h2>Categories</h2>
        <ul class='cats'>
            <xsl:call-template name='process-cats' />
        </ul>
        <p><a href='?action=cat-edit'>New</a></p>

    </xsl:template>

    <xsl:template name='process-cats'>
        <xsl:for-each select='/*/categories/item'>
            <xsl:sort select="@name"/>
            <xsl:variable name="name" select="@name" />
            <li class='cat-lvl'>
                (<xsl:value-of select="$name" />)
                <a href='?action=cat-edit&amp;name={$name}'>
                    <xsl:value-of select='.' />
                </a></li>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name='process-item'>
      <xsl:variable name="isgone" select="status" />
	  <tr>
            <td>
		
		<xsl:choose>
		<xsl:when test="string($isgone)='deleted' or string($isgone)='duplicate' or string($isgone)='ready'"><i>
		<!-- <del> commented out 07-21-10 by Leon Lin -->
                <xsl:value-of select="submitter" /><br />
                Status: <span class="highlight"><xsl:value-of select="status" /></span><br />
                <xsl:variable name="cat" select="category" />
                Cat: <a href='?action=cat-edit&amp;name={$cat}'>
                    <xsl:value-of select='/*/categories/item[@name=string($cat)]' /></a><br />
                <xsl:variable name="idnum" select="@idnum" />
                <a href="?action=edit&amp;id={$idnum}">Edit</a>
		<!-- </del> --></i>
		</xsl:when>
		<xsl:otherwise>
		                <xsl:value-of select="submitter" /><br />
                Status: <xsl:value-of select="status" /><br />
                <xsl:variable name="cat" select="category" />
                Cat: <a href='?action=cat-edit&amp;name={$cat}'>
                    <xsl:value-of select='/*/categories/item[@name=string($cat)]' /></a><br />
                <xsl:variable name="idnum" select="@idnum" />
                <a href="?action=edit&amp;id={$idnum}">Edit</a>
		</xsl:otherwise>
		</xsl:choose>
            </td>
            
            <!-- Some stuff was changed by Leon Lin 07-21-10 -->
            <xsl:choose>
            <xsl:when test="string($isgone)='duplicate' or string($isgone)='deleted'">
                <td bgcolor="#444444">
                 
                <dl class="item quote">
                    <xsl:if test="string-length(context) != 0">
                        <dt class="special context">Comments</dt>
                        <dd><xsl:value-of select="context" /></dd>
                        <hr align="left" width="500"></hr>
                    </xsl:if>
                    <xsl:for-each select="line">
                        <dt>
			    <i><del>
			        <xsl:value-of select="person" />
                                <xsl:if test="string-length(context) != 0">
                                    <span class="context"> (<xsl:value-of select="context" />)</span>
                                </xsl:if>
                            </del></i>
			    </dt>
                        <dd>
			    
			    <i><del>
			    <xsl:value-of select="text" />
			    </del></i>
			    
			    </dd>
                    </xsl:for-each>
                </dl>
                </td>
            </xsl:when>
            <xsl:when test="string($isgone)='ready'">
                <td bgcolor="#00CCFF">
                 
                <dl class="item quote">
                    <xsl:if test="string-length(context) != 0">
                        <dt class="special context">Comments</dt>
                        <dd><xsl:value-of select="context" /></dd>
                        <hr align="left" width="500"></hr>
                    </xsl:if>
                    <xsl:for-each select="line">
                        <dt>
			    <i>
			        <xsl:value-of select="person" />
                                <xsl:if test="string-length(context) != 0">
                                    <span class="context"> (<xsl:value-of select="context" />)</span>
                                </xsl:if>
                            </i>
			    </dt>
                        <dd>
			    
			    <i>
			    <xsl:value-of select="text" />
			    </i>
			    
			    </dd>
                    </xsl:for-each>
                </dl>
                </td>
            </xsl:when>
            <xsl:otherwise>
                <td>
                 
                <dl class="item quote">
                    <xsl:if test="string-length(context) != 0">
                        <dt class="special context">Comments</dt>
                        <dd><xsl:value-of select="context" /></dd>
                        <hr align="left" width="500"></hr>
                    </xsl:if>
                    <xsl:for-each select="line">
                        <dt>
			
			
			     <xsl:value-of select="person" />
                            <xsl:if test="string-length(context) != 0">
                                <span class="context"> (<xsl:value-of select="context" />)</span>
                            </xsl:if>
 			
			    </dt>
                    <dd>
			        <xsl:value-of select="text" />
			        </dd>
                        </xsl:for-each>
                    </dl>
                </td>
            </xsl:otherwise>
            </xsl:choose>
            <!-- <td> -->
           
        </tr>
    </xsl:template>
</xsl:transform>
