<?PHP

### The following should not need adjusting once it is set up

# All password related stuff is written in a file not modified by
# configure or init.
require('passwords.php');

# Maximum people participating in a quote
$maxpeopleperquote=5;

# Where the yearbook home directory is. No trailing slash.
$ybhomedir='/home/oem/yb';

# Where to find the pages/ directory. No trailing slash.
$pagesprefix="$ybhomedir/pages";

# Where to find the utils/ directory. No trailing slash.
$utilprefix="$ybhomedir/utils";

# Where the php files are located.
$phpprefix="$ybhomedir/html";

# Where the html files are located.
$htmlprefix="$ybhomedir/html";

# Where the php library files are located.
$phplibprefix="$phpprefix/lib";

# Name of this year's techie
$techiename='Richard';

$curPath = ini_get('include_path');
$newPath = implode(array(
    $phplibprefix,
    $phpprefix,
    $curPath), PATH_SEPARATOR);
ini_set('include_path', $newPath);
?>
