<?php

### Passwords
# These can be changed at any time and will take effect immediately

# Do we require passwords?
$usepasswords=false;

# What is the Yearbook Staff password?
$passwords['staff']='staffpassword';

$passwords['quotes']='quotespassword';

# What is the unlocking password?
$passwords['unlock']='unlockpassword';



# What is the password for people having a chance to do things after
# the deadline, but which doesn't allow them to touch "final" versions?
$passwords['late']='latepassword';

# Mathcamp staff password, for editing staff recommendations
$passwords['mcstaff'] = 'mcstaffpassword';

# Photo editor/uploader password
$passwords['photoeditor'] = 'photopassword';

# Photo admin password
$passwords['photoadmin'] = 'photoadminpassword';

?>
