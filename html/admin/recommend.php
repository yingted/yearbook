<?php
require_once('../config.php');
require_once('lib/recommend.php');
$loggedin = require_priv('rec');
?>
<html>
<head>
<title>Recommendation admin</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php

// Determine the what to do
$actions = array('edit', 'list', 'cat-edit');
$action = $_REQUEST['action'];
if(!in_array($action, $actions)) $action = 'list';

$recs = new Recommendation();
$recs->maybe_galley();

// Do the action
if($action == 'list')
{
    print "<h1>Recommendation list</h1>";
    print $recs->get_list();
} else if($action == 'edit') {
    $id = intval($_REQUEST['id']);
    print "<h1>Editing recommendation #$id</h1>\n";
    $recs->do_edit_form($id);
} else if($action == 'cat-edit') {
    print_r($_REQUEST);
    $name = clean_space($_REQUEST['name']);
    $oldname = clean_space($_REQUEST['oldname'] ? 
        $_REQUEST['oldname'] : $_REQUEST['name']);
    print "<h1>Editing recommendation category $name</h1>\n";
    $recs->do_cat_edit_form($oldname);
}
?>
</body>
</html>
