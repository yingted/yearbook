<?php
require_once('photolib.php');

#if (isset($_REQUEST['pass'])) {
#    $pass = $_REQUEST['pass'];
#    if ($pass === $passwords['photoeditor']) {
#        $_SESSION['id'] = 'photoeditor';
#        $_SESSION['idtype'] = 'photoeditor';
#    } else if ($pass === $passwords['photoadmin']) {
#        $_SESSION['id'] = 'photoadmin';
#        $_SESSION['idtype'] = 'photoadmin';
#    }
#}

checkperms();

?>
<?php
if($loggedin)
{
    ?>
<html>
<head>
<title>Yearbook photos</title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<h1>Welcome to Yearbook Photo Stuff</h1>

<ul>
<li><a href="everyone.php">All people</a></li>
<li><a href="camera.php">For Photographers</a></li>
<li><a href="editor.php">For Editors</a></li>
<li><a href="complain.php">Complainers</a></li>
</ul>

<p>Website broken? Poke a techie to see what's up.</p>

<p><a href="..">Back to index</a></p>

</body>
</html>
    <?php
} else {
    draw_stupid_login_form();
}
?>
