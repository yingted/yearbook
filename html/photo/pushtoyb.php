<?php

require_once('photolib.php');

// vim: set sw=4 sts=4:

checkperms();
$loggedin or die("Not logged in.");
$isadmin or die("Need permissions.");

$person = $_GET['person'];
$photo = $_GET['photo'];
$editname = $_GET['edit'];

if (!array_key_exists($person, $people)) {
    die("Person $person does not exist.");
}

$pdata = new Person($person);
$locked = $pdata->is_locked();

(!$locked) or die("Entry is locked");

$pcur = $pdata->get_photo($photo) or die("Photo does not exist.");
$pedit = photo_get_edit($pcur,$editname) or die("Edit does not exist.");

$status = $pedit->getAttribute('status');

$status == 1 or die("Photo is not okayed.");

$pdata->push_yb_photo($editname);

header("Location: viewperson.php?person={$person}");

?>
