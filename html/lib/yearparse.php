<?PHP
# yearparse.php
# Extract information from the .xml yearbook entries.
# The files use the yearbook-entry DTD, which is essentially identical to
# the LaTeX class file (allowing almost trivial translation)

# This code is based on a mixture of www.php.net and
# http://www.zend.com/zend/art/parsing.php

# It's also horrible code as it essentially collapses the beautifully
# structured XML file to a flat mess.  We can only do it because no element
# names are repeated.  Do a better job than this if you want to do anything
# more sophisticated!

$reg_fields = array('indexentry'     => 1,
                    'registeredname' => 1,
                    'name'        => 1,
                    'address'     => 1,
                    'phonenum'       => 1,
                    'msn'         => 1,
                    'livejournal' => 1,
                    'birthday'    => 1,
                    'school'      => 1,
                    'main-entry'  => 1,
                    'version'     => 1,
                    'comment'     => 1);
$multi_fields = array('email'     => 1,
                      'aim'       => 1,
                      'icq'       => 1);

function read_entry($filename)
{
  global $reg_fields, $multi_fields;

  # read the XML camper entry
  $data = implode("", file($filename));
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 0);
  $ret = xml_parse_into_struct($parser, $data, $values, $tags);
  xml_parser_free($parser);
  if (! $ret) { return False; }
  # print_r($values);
  # print_r($tags);

  # loop through the structures
  foreach ($tags as $key=>$val) {
    # Got to handle these ones separately
    if (isset($multi_fields[$key])) {
      $ranges = $val;
      # the values associated with 'email' are the locations of the email
      # tag in the values array
      foreach ($ranges as $i) {
	$tdb["${key}s"][] = trim($values[$i]["value"]);
      }
    } elseif ($key === "photo") {
      $ranges = $val;
      for ($i=$ranges[0] + 1; $i < $ranges[count($ranges)-1]; $i++) {
	if ($values[$i]["tag"] != "photo")
	  $tdb[$key][$values[$i]["tag"]] = trim($values[$i]["value"]);
      }
    } elseif ($key === 'extrabiog') {
      $ranges = $val;
      foreach ($ranges as $i) {
        $tdb[$key][$values[$i]["attributes"]["fieldname"]] =
          trim($values[$i]["value"]);
      }
    } elseif (isset($reg_fields[$key])) {
      $i = $val[0];
      $tdb[$key] = trim($values[$i]["value"]);
    } else {
      continue;
    }
  }

  /*
  # Parse photo stuff
  #print_r($tags);
  #print_r($values);
  if (isset($tags['picture-status'])) {

      # Get attributes
      $start = $tags['picture-status'][0];
      $ps = array();
      $ps['locked'] = intval($values[$start]['attributes']['locked']);
      $ps['requestnew'] = intval($values[$start]['attributes']['requestnew']);

      # Get the complaint
      if (isset($tags['pic-complaint'])) {
	  $ps['pic-complaint'] = $values[$tags['pic-complaint'][0]]['value'];
      }

      # Get the pictures
      $pics = array();
      $thepic = 0;
      if (isset($tags['picture'])) {
	  foreach ($tags['picture'] as $pid) {
	      if ($values[$pid]['type'] != 'open') // skip extra junk
		  continue;
	      $pic = array();

	      # Get attributes
	      $pic['filename'] = $values[$pid]['attributes']['filename'];
	      if (intval($values[$pid]['attributes']['current']))
		  $ps['current'] = $thepic;

	      $pic['edits'] = array();
	      # Parse inside tags
	      $e = count($values); $stop = $values[$pid]['level'];
	      for ($i=$pid+1; $i<$e; $i++) {
		  if ($values[$i]['level'] <= $stop && $values[$i]['type'] != 'cdata') // php4 xml == bad
		      break;
		  if ($values[$i]['tag'] == 'assign') { // Assignments
		      // TODO: Support multiple assignments?
		      $pic['assign-to'] = $values[$i]['value'];
		      $pic['assign-at'] = intval($values[$i]['attributes']['time']);
		  } elseif ($values[$i]['tag'] == 'edit') { // Edits
		      $edit = array();
		      $edit['status'] = intval($values[$i]['attributes']['status']);
		      $edit['editor'] = $values[$i]['attributes']['editor'];
		      $edit['filename'] = $values[$i]['value'];

		      array_push($pic['edits'], $edit);
		  }
	      }

	      array_push($pics, $pic);
	      $thepic++;
	  }
      } else {
	  // we need pictures, sanity filter
	  $ps['locked'] = 0;
	  $ps['requestnew'] = 1;
      }
      $ps['pictures'] = $pics;
      #print_r($ps);
      $tdb['picture-status'] = $ps;
  } else {
      $tdb['picture-status'] = array('locked' => 0, 'requestnew' => 1, 'pictures'=>array());
  }
   */

  return $tdb;
}

# For testing
# require('config.php');
# $db = read_entry("$pagesprefix/entries/Alex_Dehnert2.xml");
# print_r($db);

?>
