<?php

// vim: set sw=4 sts=4:

require_once('photolib.php');

checkperms();

if($loggedin) {

    $isadmin or die("Not an admin");

    //die("Foo\n");

    $person = $_GET['person'];
    $photo = $_GET['photo'];
    $editname = $_GET['edit'];

    if (!array_key_exists($person, $people)) {
        die("Person $person does not exist.");
    }

    $pdata = new Person($person);
    $locked = $pdata->is_locked();

    (!$locked) or die("Entry is locked");

    $pcur = $pdata->get_photo($photo);
    $pedit = photo_get_edit($pcur,$editname);

?>
<html>
<head>
<title>Moderate an Edit</title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<p><a href="index.php">Back to photos</a></p>

<h1><?echo $people[$person]?></h1>

<a href="img/<?echo $pedit->textContent?>.jpg"><img src="img/thumb/<?echo $pedit->textContent?>.jpg" /></a>
<p>Edited by <?echo $pedit->getAttribute('editor');?>.</p>

<form action="modedit.php" method="post">
<select name="status">
<?
    $status = array('Reject', 'Unmoderated', 'Accept');
    $cur_s = $pedit->getAttribute('status');
    for ($i=-1;$i<count($status)-1;$i++) {
        echo "<option value=\"{$i}\" ";
        if ($cur_s == $i)
            echo 'selected="selected" ';
        echo ">{$status[$i+1]}</option>\n";
    }
?>
<input type="hidden" value="<? echo $person ?>" name="person" />
<input type="hidden" value="<? echo $photo ?>" name="photo" />
<input type="hidden" value="<? echo $editname ?>" name="editname" />
<input type="submit" value="Moderate" name="sub" />
</select>
</form>

</body>
</html>
<?
} else {
        draw_stupid_login_form();
    }
?>
