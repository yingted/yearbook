<?php

require_once('photolib.php');

checkperms();
$loggedin or die("Not logged in.");

$person = $_POST['person'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

isset($_FILES['photo']) or die("No photo uploaded.");
(strtolower(substr($_FILES['photo']['name'], -4)) == '.jpg') or die("Not a JPEG file.");

$filename = time() . '-' . rand(100, 999);
move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir.'/'.$filename.'.jpg') or die("Failed to move uploaded file.");
chmod($uploaddir.'/'.$filename.'.jpg', 0664);
gen_thumb($filename);

// Unset the other currents
foreach ($pdata->get_photos() as $photo) {
    $photo->setAttribute('current', 0);
    // avoid breakage
    foreach ($photo->getElementsByTagName('assign') as $assign) {
	$assign->parentNode->removeChild($assign);
    }
}

// Now, create the node
$pdb = PersonDB::get();
$node = $pdb->dom->createElement('picture');
$node->setAttribute('filename', $filename);
$node->setAttribute('current', 1);
$pdata->picturestatus->appendChild($node);
$pdb->dirty();

// Update categories
$cats = Categories::get();
$cats->add_person_to('unassigned', $person);
$cats->del_person_from('needphoto', $person);
$cats->del_person_from('requestnew', $person);

$pdata->picturestatus->setAttribute('requestnew', 0);

header("Location: viewperson.php?person={$person}");

?>
