<?php
require_once(dirname(__FILE__) . '/../config.php');
require_once('xml-lib.php');
require_once('year_lib.php');

class Quote extends DataFileManager
{
    var $progname = 'quote';
    var $name = 'quote';
    var $maxpeople = 5;
    function __construct()
    {
        parent::__construct();
    }

    function get_form($id, $admin = false)
    {
    	 // General setup
        $ret = '';
        $item = $this->get_item($id);
        $xpath = $this->xpath;
        ob_start();

        // Individual lines
        print "<tr><th colspan='2'><h2>Lines</h2></th></tr>\n";
        $lines = $xpath->query("line", $item);
        $i = 0;
        foreach($lines as $line)
        {
            $i++;
            $text = $xpath->query("text", $line)->item(0)->nodeValue;
            $person = $xpath->query("person", $line)->item(0)->nodeValue;
            $context = $xpath->query("context", $line)->item(0)->nodeValue;
            print $this->get_quote_line($i, $text, $person, $context, $i);
        }
        if($admin) print $this->get_quote_line(1001, '', '', '', $i+1);

        print "<tr><th colspan='2'><h2>General</h2></th></tr>\n";

        // Context
        $context = $xpath->query("context", $item);
        if($context->length > 0) $context = $context->item(0)->nodeValue;
        else $context = '';
        tab_item("Comments", 'context', $context);

        print $this->form_append($item, $id, $admin);
        $ret .= ob_get_contents();
        ob_end_clean();

        return $ret;
    }

    function get_quote_line($i, $text, $person, $context='', $iprime=1001)
    {
        $ret = '';
        $ret .= "<tr><th>Line $iprime</th><td>\n";
        $ret .= "<div class='quote-line'>";
        if ($i>1) $ret .= "Then ";
	$context_escaped = htmlspecialchars($context, ENT_QUOTES);
	$person_escaped = htmlspecialchars($person, ENT_QUOTES);
	$text_escaped = htmlspecialchars($text, ENT_QUOTES);
        $ret .= "
            <input type=\"text\" name=\"quoted$i\" value=\"$person_escaped\"> said <BR>
            (in context <input type=\"text\" name=\"context$i\" value=\"$context_escaped\">)<BR>
            <textarea name=\"quote$i\" rows=\"4\" cols=\"30\">$text_escaped</textarea><BR>
            </div>\n";
        $ret .= "</td></tr>\n";
        return $ret;
    }

    function create_new_node($id)
    {
        $element = $this->doc->createElement($this->progname);
        if($id==0) $id = $this->incr_curid();
        $exists = false;
        for ($i=1; $i<=$this->maxpeople; $i++) {
            $exists = $this->create_new_node_line($i, $element, $exists);
        }
        if($_REQUEST["quote1001"])
            $exists = $this->create_new_node_line(1001, $element, $exists);

        if ($exists) {
            $this->create_new_node_std($element, $id);
            $gencontext = clean_space(input_unescape_slashes($_REQUEST['context']));
            if($gencontext)
                $element->appendChild($this->doc->createElement('context', $gencontext));
            $element->appendChild($this->doc->createTextNode("\n"));
        }
        return $element;
    }
    
    function create_new_node_line($i, $element, $exists = false)
    {
        $quote = clean_space(input_unescape_slashes($_REQUEST["quote$i"]));
        $quoted = clean_space(input_unescape_slashes($_REQUEST["quoted$i"]));
        $context = clean_space(input_unescape_slashes($_REQUEST["context$i"]));
        if ($quote != "" && $quoted != "") {
            if (! $exists) {
                $exists = True;
                $element->appendChild($this->doc->createTextNode("\n"));
            }
            $line = $this->doc->createElement('line');
            $line->appendChild($this->doc->createElement('person', $quoted));
            $line->appendChild($this->doc->createElement('text', $quote));
            if($context)
                $line->appendChild($this->doc->createElement('context', $context));
            $element->appendChild($line);
            $element->appendChild($this->doc->createTextNode("\n"));
        }
        return $exists;
    }
}
?>
