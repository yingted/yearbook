<?php

require_once('photolib.php');

checkperms();

?>
<html>
<head>
<title>Photographers</title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<p><a href="index.php">Back to photos</a></p>
<?php
if($loggedin)
{
    ?>
<h1>Photographers</h1>

<?
    $categories = Categories::get();

    echo '<h2>People Who Need Pictures</h2>';
    // FIXME: A bit of waste of space, but this looks better. :-D
    $categories->display_blocks_by_group('needphoto', 'thin');
    /*
    $needphoto = $categories->get_people('needphoto');
    echo "<ul>\n";
    foreach ($needphoto as $person) {
	echo "  <li><a href=\"viewperson.php?person=$person\">{$people[$person]}</a></li>\n";
    }
    echo "</ul>\n";
     */


    echo '<h2>People Who Requested New Pictures</h2>';
    $categories->display_blocks_by_group('requestnew');

?>

<?php
} else {
?>
    <h1>Not logged in</h1>
    <p>You are not logged in. If you are on photo staff, enter a photo staff
    password to log in. Otherwise, you shouldn't need to see this page.</p>

    <form method="post" action=".">
    <label>Password: <input type="password" name="pass" /></label>
    </form>
<?php
}
?>
</body>
</html>
