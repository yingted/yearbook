<?php
require_once('config.php');
require_once('year_lib.php');
require_once('recommend.php');
umask(002);

print start_html("Recommendation");
?>

<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>Recommendation</h1>
<h2>Instructions</h2>

<p>Please include your full name (under "submitter"), so that we can
find you if we need more information.</p>

<p>Give some idea of what kind of recommendation this is (a graduate
level math textbook? a non-technical introduction? music?), as well
as any other comments in the "Comments" field.</p>

<h2>The Recommendation</h2>

<?php
$recs = new Recommendation();
$id = intval($_REQUEST['id']);
$recs->do_edit_form($id);
?>
</body>
</html>
