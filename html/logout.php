<?PHP
require('config.php');

session_start();

$_SESSION = array();
if (isset($_COOKIE[session_name()])) {
  setcookie(session_name(), '', time()-42000, '/');
}

// Finally, destroy the session.
session_destroy();

print start_html("Yearbook log out");
?>

<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>Mathcamp Yearbook log out</h1>

<p>You have successfully logged out!  Click <a href="index.php">here</a>
to go back to the Yearbook homepage.</p>

</body>
</html>
