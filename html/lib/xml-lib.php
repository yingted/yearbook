<?php
// Initialization
require_once(dirname(__FILE__) . '/../config.php');
require_once('year_lib.php');

class DataFileManager
{
    /**
     * Name to use for programatic purposes
     *
     * For example, CSS classes or file names
     */
    var $progname = 'manager';

    /**
     * Human-readable name
     */
    var $name = 'manager';

    /**
     * Transform the XML file into HTML list
     */
    var $adminListXSL = "transform.xsl";

    /**
     * Constructor --- load, lock, etc.
     */
    function __construct()
    {
        global $pagesprefix;
        global $techiename;
        if($this->progname)
        {
            $this->adminListXSL = "{$this->progname}s-to-admin-list.xsl";
            $this->lockfile = "{$pagesprefix}/{$this->progname}s.lock";
            $this->lock();
            $this->file = "{$pagesprefix}/{$this->progname}s.xml";
            $this->doc = new DomDocument();
            $this->doc->load($this->file);
            if(!$this->doc) {
                exit("I can't open the {$this->name} file. This is bad. Find $techiename as soon as possible and do anything you can to get him to fix it!");
            }
            $this->xpath = new DOMXPath($this->doc);
            $this->root = $this->doc->getElementsByTagName("{$this->progname}s");
            $this->root = $this->root->item(0);
            $this->items = $this->xpath->query('items', $this->root)->item(0);
        }
        $this->statuses = array('ready', 'submit', 'duplicate', 'deleted', 'supplement', 'preexistent');
    }

    /**
     * Shutdown function --- cleanup.
     */
    function __destruct()
    {
        $this->unlock();
    }

    /**
     * Lock the datafile
     */
    function lock()
    {
        global $techiename;
        $this->lockobj = fopen($this->lockfile,"r");
        if (!flock($this->lockobj,LOCK_EX)) {
            exit("Someone seems to have submitted a {$this->name} at the same time as you. Please try reloading the page; if you still see this message, find $techiename.");
        }
        $this->locked = true;
    }

    /**
     * Unlock the datafile
     */
    function unlock()
    {
        $lock = $this->lockobj;
        if($this->locked)
        {
            flock($lock,LOCK_UN);
            fclose($lock);
        }
        $this->locked = false;
    }

    /**
     * Get the "list" of items for display in admin.
     */
    function get_list($params = array())
    {
        $listxsl = new DOMDocument;
        $ret = $listxsl->load($this->adminListXSL);
        if(!$ret) return false;

        // Configure the transformer
        $proc = new XSLTProcessor();
        foreach($params as $name => $val)
        {
            $proc->setParameter('', $name, $val);
        }
        $proc->importStyleSheet($listxsl); // attach the xsl rules

        return $proc->transformToXML($this->doc);
    }

    /**
     * Get an item by id
     */
    function get_item($id)
    {
        $xpath = $this->xpath;
        $query = "/{$this->progname}s/items/{$this->progname}[@idnum=$id]";
        $item = $xpath->query($query)->item(0);
        return $item;
    }

    /**
     * Get the value of a child element
     */
    function get_cv($name, $base)
    {
        //try {
        if($base == "") { echo $name; }
            $xresult = $this->xpath->query($name, $base);
            if($xresult->length > 0) $ret = $xresult->item(0)->nodeValue;
            else $ret = '';
            return $ret;
        //}
    }

    /**
     * Get a status select box for edit form
     */
    function make_status_select($status, $choices = false)
    {
        $ret = '';
        $ret .= "<tr><td>Status</td><td><select name='status'>\n";
        if(!$choices) $choices = $this->statuses;
        foreach($choices as $cur)
        {
            $ret .= "<option value='$cur'";
            if($cur == $status) $ret .= " selected";
            $ret .= ">$cur</option>\n";
        }
        $ret .= "</select></td></tr>\n";
        return $ret;
    }

    /**
     * Get a category select box for edit form
     */
    function make_cat_select($cat)
    {
        $ret = '';
        $ret .= "<tr><td>Category</td><td><select name='category'>\n";
        if (!$cat) $cat = '';
        $cats = $this->get_categories();
        foreach($cats as $cur)
        {
            $name = $cur['name'];
            $level = substr_count($name, '/');
            $label = $cur['label'];
            $disp = str_repeat('&#160;', 2*$level) . $label . " ($name)";
            $ret .= "<option value='$name'";
            if($name == $cat) $ret .= " selected";
            $ret .= ">$disp</option>\n";
        }
        $ret .= "</select></td></tr>\n";
        return $ret;
    }

    /**
     * Get form for editing.
     *
     * Should be overridden
     */
    function get_form($id)
    {
    }

    /**
     * Get and increment the current id
     */
    function incr_curid()
    {
        $curid = $this->root->getAttribute('curid');
        $this->root->setAttribute('curid', intval($curid)+1);
        return $curid;
    }

    /**
     * Append standard things to the edit form
     */
    function form_append($item, $id, $admin = false)
    {
        ob_start();
        // Handle status
        if($admin)
        {
            $status = $this->get_cv("status", $item);
            print $this->make_status_select($status);
        }

        // Handle categories
        if($admin)
        {
            $cat = $this->get_cv("category", $item);
            print $this->make_cat_select($cat);
        }

        // Submitter
        $submitter = $this->get_cv("submitter", $item);
        tab_item("Submitter", 'submitter', $submitter);
        $ret .= ob_get_contents();
        ob_end_clean();

        $ret .= "<input type='hidden' name='id' value='$id' />";
        $ret .= "<input type='hidden' name='action' value='edit' />";
        $ret .= "<tr><td></td><td><input type='submit' name='submit' value='Save' /></td></tr>";
        return $ret;
    }

    /**
     * Create standard parts of a item's XML node
     */
    function create_new_node_std($element, $id)
    {
        $status = clean_space($_REQUEST['status']);
        $category = clean_space($_REQUEST['category']);
        $submitter = clean_space($_REQUEST['submitter']);
        $element->appendChild($this->doc->createElement('category', $category));
        $element->appendChild($this->doc->createElement('status', $status));
        $element->appendChild($this->doc->createElement('submitter', $submitter));
        $element->setAttribute('idnum', $id);
    }

    /**
     * Get the list of categories
     */
    function get_categories()
    {
        $ret = array();
        $xpath = $this->xpath;
        $catsnodes = $xpath->query("/*/categories/item");
        foreach($catsnodes as $cat)
        {
            $ret[] = array(
                'label' => $cat->nodeValue,
                'name'  => $cat->getAttribute('name'));
        }
        usort($ret, array($this, 'cmp_name'));
        return $ret;
    }

    /**
     * Compare an array by checking name element
     */
    function cmp_name($a, $b)
    {
        return strcmp($a['name'], $b['name']);
    }

    /**
     * Create a new XML node.
     *
     * This should be overridden
     */
    function create_new_node($id)
    {
    }

    /**
     * Save the XML file
     */
    function save()
    {
        global $utilprefix;
        $this->doc->save($this->file);
        $this->unlock();
        system("$utilprefix/cvs {$this->file}");
    }

    /**
     * Show edit form and save submitted stuff
     */
    function do_edit_form($id)
    {
        $admin = user_can($this->progname, 'edit');
        if(isset($_REQUEST['submit']))
        {
            $newnode = $this->create_new_node($id);
            $oldnode = $this->get_item($id);
            if($oldnode && $id > 0) $this->items->replaceChild($newnode, $oldnode);
            else {
                $this->items->appendChild($newnode);
                $this->items->appendChild($this->doc->createTextNode("\n"));
                $this->items->appendChild($this->doc->createTextNode("\n"));
            }
            print "<div class='success'><p>You successfully posted</p></div>\n";
            $this->save();
        }
        print "<form method='post'><table>\n";
        print $this->get_form($id, $admin);
        print "</table></form>\n";
        if($admin)
        {
            $curid = $this->root->getAttribute('curid');
            print "<p>Return to <a href='?action=list'>listing</a>";
            if ($id+1 < $curid)
                print " or go to the <a href='?action=edit&amp;id=" . ($id+1)
                    . "'>next one</a>";
            print "</p>\n";
        }
    }

    /**
     * Show edit form for categories and save submitted stuff
     */
    function do_cat_edit_form($name)
    {
        $admin = user_can($this->progname, 'cat-edit');
        $node = false;
        if($name) $node = $this->get_cat_item($name);
        if(!$node) $node = $this->create_new_cat_node();
        if(isset($_REQUEST['submit']))
        {
            $this->update_cat_node($node);
            $this->save();
        }
        print "<form method='post'><table>\n";
        print $this->get_cat_form($node);
        print "</table></form>\n";
        print "<p>Return to <a href='?action=list'>listing</a></p>\n";
    }

    /**
     * Get form for editing categories.
     */
    function get_cat_form($item)
    {
        // Setup
        $ret = '';
        $xpath = $this->xpath;
        ob_start();

        // Main
        $label = $item->nodeValue;
        tab_item("Label", 'label', $label);
        $name = $item->getAttribute('name');
        tab_item("Name", 'name', $name);

        // End
        $ret .= ob_get_contents();
        ob_end_clean();
        $ret .= "<tr><td></td><td><input type='hidden' name='oldname' value='$name' /></td></tr>\n";
        $ret .= "<tr><td></td><td><input type='submit' name='submit' value='Save' /></td></tr>\n";
        return $ret;
    }

    /**
     * Create standard parts of a category's XML node
     */
    function create_new_cat_node($name = '')
    {
        $element = $this->doc->createElement('item');
        $this->xpath->query('/*/categories')->item(0)->appendChild($element);
        return $element;
    }

    function update_cat_node($element)
    {
        $name = clean_space($_REQUEST['name']);
        $label = clean_space($_REQUEST["label"]);
        $element->setAttribute('name', $name);
        $element->nodeValue = $label;
        return $element;
    }

    /**
     * Get a category by name
     */
    function get_cat_item($name)
    {
        $xpath = $this->xpath;
        $query = "/*/categories//item[@name='$name']";
        $item = $xpath->query($query)->item(0);
        return $item;
    }

    function maybe_galley()
    {
        $name = "{$this->progname}s";
        if(user_can($this->progname, 'regen'))
        {
            print "<p><a href='galleys.php?galley=$name&amp;action=regen'><em>Regenerate galley</em></a></p>\n";
            print "<p><a href='../pdfs/galley-$name.pdf'><em>View galley</em></a></p>\n";
        }
    }
}
?>
