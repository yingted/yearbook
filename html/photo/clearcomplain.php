<?php

require_once('photolib.php');

checkperms();
$loggedin or die("Not logged in.");

$person = $_GET['person'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

$pdata->clear_complaint();

header("Location: viewperson.php?person={$person}");

?>
