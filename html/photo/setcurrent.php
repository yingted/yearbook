<?php

// vim: se sw=4 sts=4:

require_once('photolib.php');

checkperms();
$loggedin or die("Not logged in.");

$person = $_GET['person'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$filename = $_GET['photo'];

$pdb = PersonDB::get();

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

//$photos = $pdata->get_photos();
$photo = $pdata->get_photo($filename);
$pnum = count($photos);
//foreach ($photos as $photo) {
    //if ($photo->getAttribute('filename') == $filename) {
	$curphoto = $pdata->get_photo_current();
	$curphoto->setAttribute('current', 0);
	$photo->setAttribute('current', 1); 
	$pdata->dirty_photos();
	$pdb->dirty();

	$pdata->reset_categories();

	//break;
    //}
//}

header("Location: viewperson.php?person={$person}");

?>
