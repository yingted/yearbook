<?php
require_once('../config.php');
require_once('lib/year_lib.php');

$galleys = array(
    'campers',
    'intro',
    'preface',
    'quotes',
    'recs',
    'staff',
    'staffrecs',
//    'winners',
    'yb_staff',
    'misc',
);
?>
<html>
<head>
<title>PDFs for the yearbook</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<h1>Galleys</h1>

<?php
$galley = $_REQUEST['galley'];
$action = $_REQUEST['action'];
if($action and in_array($galley, $galleys))
{
    if(strrpos($galley, 's'))
	    $galleypriv = substr($galley, 0, -1);
    else $galleypriv = $galley;
    if(user_can($galleypriv, 'regen') or user_can('galley', 'regen'))
    {
        print "<div class='success'>";
        list ($output, $retval) = update_galley($galley);
	print $output;
	if ($retval == 0) {
	    print "<p>You regenerated the galley</p>\n";
            print "<p><a href='../pdfs/galley-$galley.pdf'>View</a></p>\n";
            print "</div>\n";
     	} else {
	    print "<p>Regenerating the galley failed.  Please fix the problem before trying again.</p>\n";
            print "</div>\n";
	}
    } else print "<p>You can't regenerate that galley</p>";
} else print "<p>Just listing</p>";
?>
<ul>
<?php
foreach($galleys as $galley)
{
    print "<li>";
    print "<a href='../pdfs/galley-$galley.pdf'>";
    print "$galley";
    print "</a> ";
    print "(<a href='?galley=$galley&amp;action=regen'>Regenerate</a>)";
    print "</li>\n";
}
?>
<li><a href='../pdfs/yearbook.pdf'>Full yearbook</a>
</ul>
</body>
</html>
