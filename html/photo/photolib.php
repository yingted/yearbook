<?

// vim: set sw=4 sts=4:

require_once('../config.php');
require_once('year_lib.php');
require_once('people.php');


$uploaddir = $phpprefix.'/photo/img';
$finaldir = $ybhomedir.'/pics';
$thumbsize = 125;

function draw_stupid_login_form() {
    require_login('photo', '../');
}


function checkperms() {
    global $isadmin, $loggedin;
    $isadmin = user_can('photo', 'admin');
    $loggedin = $isadmin || user_can('photo', 'edit');
}

// Because textContent seems read-only
function replace_text($node, $text) {
    foreach ($node->childNodes as $child) {
        $node->removeChild($child);
    }
    $node->appendChild($node->ownerDocument->createTextNode($text));
}


// Stupid lack of line breaks
function addbr($node) {
    $node->appendChild($node->ownerDocument->createTextNode("\n"));
}

class Person {
    public $filename;

    public $domelement;
    public $picturestatus;

    public $pictures;
    public $pcurrent;

    public $categories;
    private $cdirty;

    public function __construct($data) {
        $db = PersonDB::get();
        $this->domelement = $db->get_data($data);
        $this->picturestatus = $this->domelement->getElementsByTagName('picture-status')->item(0);
        $this->filename = $data;
        $this->cdirty = false;
    }

    public function get_complaint() {
        $complain = $this->picturestatus->getElementsByTagName('pic-complaint');
        if ($complain) {
            return $complain->item(0)->textContent;
        } else {
            return False;
        }
    }

    public function clear_complaint() {
        $locked = $this->is_locked();
        if ($locked) return;

        if ($this->get_complaint() !== False) {
            $this->gen_category_list();
	    assert(isset($this->categories['complained']));

            $plist = $this->picturestatus->getElementsByTagName('pic-complaint');
            foreach ($plist as $node) {
	        $node->parentNode->removeChild($node);
            }
            PersonDB::get()->dirty();

            $cats = Categories::get();
            $cats->del_person_from('complained', $person);
        }
    }

    public function dirty_photos() { unset($this->pictures); }
    public function get_photos() {
        if (!isset($this->pictures)) {
            $this->pictures = array();
            $pictures = $this->picturestatus->getElementsByTagName('picture');
            foreach ($pictures as $node) {
                array_push($this->pictures, $node);
                if (intval($node->getAttribute('current'))) {
                    $this->pcurrent = $node;
                }
            }
        }
        return $this->pictures;
    }
    public function get_photo_current() {
        $this->get_photos();
        return $this->pcurrent;
    }

    public function get_photo($filename) {
        foreach ($this->picturestatus->getElementsByTagName('picture') as $pic) {
            if ($filename == $pic->getAttribute('filename')) {
                return $pic;
            }
        }
        return false;
    }

    public function is_locked() {
        return intval($this->picturestatus->getAttribute('locked')) != 0;
    }

    public function print_data($pichint) {
        global $people;
        if ($this->is_locked())
            echo "<div class=\"block locked\">\n";
        else if ($pichint == 'thin')
            echo "<div class=\"block thin\">\n";
        else
            echo "<div class=\"block\">\n";
        echo "<div class=\"name\">{$people[$this->filename]}</div>\n";

        if ($this->is_locked())
            echo "<i>Locked</i>\n";

        $this->print_thumb($pichint);

        if ($complain = $this->get_complaint()) {
            echo "<div class=\"complain\">{$complain}</div>\n";
        }
        echo "</div>\n";
    }

    public function print_thumb($pichint) {
        $curphoto = $this->get_photo_current();
        if ($curphoto === NULL) {
            echo "<div class=\"pic\"><i>No picture</i></div>\n";
            return;
        }
        if ($pichint != 'photo' && ($edits = $curphoto->getElementsByTagName('edit')) !== NULL) {
            $happy = $curphoto->getAttribute('filename');
            $style = 'orig';
            $cutoff = 1;
            $max = -10;


            $extra = '';
            if (($assigned = $curphoto->getElementsByTagName('assign')->item(0)) !== NULL) {
                $datestring = date("Y-m-d g:i a", $assigned->getAttribute('time'));
                $extra =  "<div class=\"assign\">Assigned to {$assigned->textContent} at {$datestring}</div>\n";
            }

            if ($pichint == 'needokay')
                $cutoff = 0;
            foreach ($edits as $edit) {
                if ($s = intval($edit->getAttribute('status')) >= $cutoff) {
                    if ($s < $max)
                        continue;
                    $happy = $edit->textContent;
                    $style = 'edit';
                    $extra = "<div class=\"assign\">Edited by {$edit->getAttribute('editor')}</div>\n";
                    $max = $s;
                    //break;
                }
            }
            echo "<div class=\"pic $style\"><img src=\"img/thumb/{$happy}.jpg\" /></div>\n";
            echo $extra;
        } else {
            echo "<div class=\"pic orig\"><img src=\"img/thumb/{$curphoto->getAttribute('filename')}.jpg\" /></div>\n";
        }
    }


    public function gen_category_list() {
        if (isset($this->categories))
            return;
        $this->categories = array();
        $string = $this->domelement->getElementsByTagName('categories')->item(0)->textContent;
        foreach (split(',', $string) as $category) {
            if ($category != "")
                $this->categories[$category] = 1; 
        }
    }

    public function clearclaims_p($photo) {
        $pdb = PersonDB::get();
        foreach ($photo->getElementsByTagName('assign') as $assign) {
            $assign->parentNode->removeChild($assign);
            $pdb->dirty();
        }
    }
    public function clearclaims() {
        foreach ($this->get_photos() as $photo) {
            $this->clearclaims_p($photo);
        }
    }

    public function update_category_string() {
        if ($this->cdirty) {
            $pdb = PersonDB::get();
            $this->gen_category_list();

            $cnode = $this->domelement->getElementsByTagName('categories')->item(0);
            replace_text($cnode, implode(',', array_keys($this->categories)));

            $pdb->dirty();
            $this->cdirty = false;
        }
    }
    /*** REMEMBER TO CALL update_category_string() FOR CHANGES TO TAKE EFFECT ****/
    public function add_category($category) {
        $this->gen_category_list();
        if (isset($this->categories[$category]))
            return;
        $this->categories[$category] = 1;
        $this->cdirty = true;
    }
    public function del_category($category) {
        $this->gen_category_list();
        if (!isset($this->categories[$category]))
            return;
        unset($this->categories[$category]);
        $this->cdirty = true;
    }


    public function reset_categories() {
        $cat = Categories::get();
        foreach ($cat->get_category_names() as $category) {
            $cat->del_person_from($category,$this->filename);
        }
        $this->categories = array(); $this->update_category_string();

        if ($this->is_locked())
            $cat->add_person_to('locked',$this->filename);

        if ($this->get_complaint()) {
            $cat->add_person_to('complained',$this->filename);
        }

        $photos = $this->domelement->getElementsByTagName('picture');
        if ($photos->length) { // has a photo

            if ($this->domelement->getAttribute('requestnew')) // request a new one
                $cat->add_person_to('requestnew',$this->filename);

            $curphoto = $this->get_photo_current();

            $needokay = false;
            $havegood = false;
            foreach ($curphoto->getElementsByTagName('edit') as $edit) { // for each edit
                $status = intval($edit->getAttribute('status'));
                if ($status == 0)
                    $needokay = true;
                else if ($status == 1)
                    $havegood = true;
            }

            if ($needokay) {
                $cat->add_person_to('needokay',$this->filename);
            } else if ($havegood) {
                $cat->add_person_to('havegood',$this->filename);
            } else {
                if ($a = photo_get_assign($curphoto)) {
                    $cat->add_person_to('assigned',$this->filename);
                } else {  // not assigned
                    $cat->add_person_to('unassigned',$this->filename);
                }
            }

        } else { // has no photo
            $cat->add_person_to('needphoto',$this->filename);
            $this->domelement->setAttribute('requestnew', 0);
        }
    }

    public function get_yb_photo() {
        global $finaldir;
        if (is_link($finaldir.'/'.$this->filename.'.jpg')) {
            $link = readlink($finaldir.'/'.$this->filename.'.jpg');
            return basename($link, '.jpg');
        } else {
            return FALSE;
        }
    }

    public function push_yb_photo($photo) {
        global $finaldir;
	global $ybhomedir;
        if (is_link($finaldir.'/'.$this->filename.'.jpg')) {
            unlink($finaldir.'/'.$this->filename.'.jpg');
        } else if (is_file($finaldir.'/'.$this->filename.'.jpg')) {
            rename($finaldir.'/'.$this->filename.'.jpg', $finaldir.'/'.$this->filename.'.jpg.old');
        }
        @unlink("$ybhomedir/html/thumbpics/{$this->filename}_thumb.jpg");

        symlink("$ybhomedir/html/photo/img/$photo.jpg", "$finaldir/{$this->filename}.jpg") or die("Couldn't symlink (1) $ybhomedir/html/photo/img/$photo.jpg $finaldir/{$this->filename}.jpg");
	symlink("$ybhomedir/html/photo/img/thumb/$photo.jpg", "$ybhomedir/html/thumbpics/{$this->filename}_thumb.jpg") or die("Couldn't symlink (2) $ybhomedir/html/photo/img/thumb/$photo.jpg $ybhomedir/html/thumbpics/{$this->filename}_thumb.jpg");
    }

}

class PersonDB {
    private static $instance;
    private $cache;
    public $dom;

    private $needflush;

    private function __construct() {
        global $phpprefix;
        $this->dom = new DOMDocument();
        $this->dom->load($phpprefix.'/photo/photo_dump.xml');
        $this->cache = array();
        $this->needflush = false;
    }

    // /** flush stuff */
    // public function __destruct() {
    //     if ($this->needflush)
    //         $this->flush_xml();
    // }
    public function flush_xml() {
        global $phpprefix;
        $this->dom->save($phpprefix.'/photo/photo_dump.xml');
        system("$utilprefix/svn $phpprefix/photo/photo_dump.xml");
        $this->needflush = false;
    }
    public function dirty() {
        $this->flush_xml();
    }

    public static function get() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    public function get_data($person) {
        if (!array_key_exists($person, $this->cache)) {
            $this->cache[$person] = $this->dom->getElementById($person);
            if (!$this->cache[$person]) // clear it
                unset($this->cache[$person]);
        }
        return $this->cache[$person];
    }

}

class Categories {
    private static $instance;
    public $dom;
    private $needflush;

    private function __construct() {
        global $phpprefix;
        $this->dom = new DOMDocument();
        $this->dom->load($phpprefix.'/photo/photo_data.xml');
        $this->needflush = false;
    }

    // /** flush stuff */
    // public function __destruct() {
    //     if ($this->needflush)
    //         $this->flush_xml();
    // }
    public function flush_xml() {
        global $phpprefix;
        $this->dom->save($phpprefix.'/photo/photo_data.xml');
        system("$utilprefix/cvs $phpprefix/photo/photo_data.xml");
        $this->needflush = false;
    }
    public function dirty() {
        $this->flush_xml();
    }

    public static function get() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    public function get_people($category) {
        $request = $this->get_category($category);
        $ans = array();
        if ($request) {
            $rlist = $request->getElementsByTagName('person');
            foreach ($rlist as $node) {
                array_push($ans, $node->textContent);
            }
        }
        sort($ans);
        return $ans;
    }

    public function get_category_names() {
        $ans = array();
        foreach($this->dom->getElementsByTagName('section') as $sec) {
            array_push($ans, $sec->getAttribute('xml:id'));
        }
        return $ans;
    }

    public function get_category($category) {
        $cat = $this->dom->getElementById($category);
        if ($cat) {
            return $cat;
        } else {
            $node = $this->dom->createElement('section');
            $node->setAttribute('xml:id', $category);
            addbr($this->dom->documentElement);
            $this->dom->documentElement->appendChild($node);
            addbr($this->dom->documentElement);
            $this->dirty();
            return $this->dom->getElementById($category);
        }
    }

    public function add_person_to($category, $person) {
        $elem = $this->get_category($category);

        $pdata = new Person($person);
        $pdata->gen_category_list();
        if (array_key_exists($category, $pdata->categories))
            return;

        $node = $this->dom->createElement('person');
        $node->appendChild($node->ownerDocument->createTextNode($person));
        addbr($elem);
        $elem->appendChild($node);

        $pdata->add_category($category);
        $pdata->update_category_string();

        $this->dirty();
    }
    public function del_person_from($category, $person) {
        $elem = $this->get_category($category);

        $pdata = new Person($person);
        $pdata->gen_category_list();
        if (!array_key_exists($category, $pdata->categories))
            return;

        foreach($elem->getElementsByTagName('person') as $p_entry) {
            if ($p_entry->textContent == $person) {
                $elem->removeChild($p_entry);
            }
        }

        $pdata->del_category($category);
        $pdata->update_category_string();

        $this->dirty();
    }

    public function display_blocks_by_group($cat, $pichint='edit', $message='None') {
        $requestnew = $this->get_people($cat);
        if (count($requestnew)) {
            echo "<ul>\n";
            foreach ($requestnew as $person) {
                $pdata = new Person($person);
                echo "<a class=\"nounderline\" href=\"viewperson.php?person={$person}\">\n";
                $pdata->print_data($pichint);
                echo "</a>\n";
            }
            echo "</ul>\n";
            echo '<br style="clear:both;" />';
        } else {
            echo "<p><i>$message</i></p>";
        }
    }

}

function photo_get_assign($photo) {
    $claims = $photo->getElementsByTagName('assign');
    if ($claims->length)
        return $claims->item(0);
    return false;
}
function photo_get_edit($photo, $editname) {
    $edits = $photo->getElementsByTagName('edit');
    foreach ($edits as $edit) {
        if ($edit->textContent == $editname) {
            return $edit;
        }
    }
    return false;
}

function shrink_image($size, $src, $dest) {
    $srcimg = ImageCreateFromJPEG($src);
    $srcX = ImageSX($srcimg);
    $srcY = ImageSY($srcimg);
    $ratio = min($size/$srcX, $size/$srcY);
    if ($ratio < 1) {
        $destX = intval($ratio*$srcX);
        $destY = intval($ratio*$srcY);
        $destimg = ImageCreateTrueColor($destX,$destY) or die("Problem In Creating image: size=$size, src=$src, dest=$dest, srcX=$srcX, srcY=$srcY, ratio=$ratio, destX=$destX, destY=$destY");
        ImageCopyResampled($destimg,$srcimg,0,0,0,0,$destX,$destY,$srcX,$srcY) or die("Problem In resizing");
        ImageJPEG($destimg,$dest) or die("Problem In saving");
    } else {
        copy($src, $dest);
    }
    chmod($dest, 0664);
}

function gen_thumb($filename) {
    global $thumbsize, $uploaddir;

    shrink_image($thumbsize, $uploaddir.'/'.$filename.'.jpg', $uploaddir.'/thumb/'.$filename.'.jpg');
}


?>
