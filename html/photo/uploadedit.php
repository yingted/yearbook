<?php

require_once('photolib.php');

// vim: set sw=4 sts=4:

checkperms();
$loggedin or die("Not logged in.");

$person = $_POST['person'];
$editor = $_POST['editor'];
$photo = $_POST['photo'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

isset($_FILES['photo']) or die("No photo uploaded.");
(strtolower(substr($_FILES['photo']['name'], -4)) == '.jpg') or die("Not a JPEG file.");

$filename = 'e' . time() . '-' . rand(100, 999);
echo("moving photo to ".$uploaddir.'/'.$filename.'.jpg'."</br>\n");
move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir.'/'.$filename.'.jpg');
chmod($uploaddir.'/'.$filename.'.jpg', 0664);
gen_thumb($filename);

// Put into CVS
// system("$utilprefix/cvs $uploaddir/$filename.jpg");
// system("$utilprefix/cvs $uploaddir/thumb/$filename.jpg");

// Get the photo
$pcur = $pdata->get_photo($photo);
$iscur = intval($pcur->getAttribute('current'));

// Clear claims
$pdata->clearclaims_p($pcur);


// Now, create the node
$pdb = PersonDB::get();
$node = $pdb->dom->createElement('edit');
$node->setAttribute('editor', $editor);
$node->setAttribute('status', 0);
$node->setAttribute('current', 1);
$node->appendChild($node->ownerDocument->createTextNode($filename));
$pcur->appendChild($node);
$pdb->dirty();

// Update categories
if ($iscur) {
    $cats = Categories::get();
    $cats->add_person_to('needokay', $person);
    $cats->del_person_from('assigned', $person);
    $cats->del_person_from('unassigned', $person);
}

header("Location: viewperson.php?person={$person}");

?>
