<?php

// vim: set sw=4 sts=4:

require_once('photolib.php');

checkperms();
$loggedin or die("Not logged in.");

$person = $_GET['person'];
$photo = $_GET['photo'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

$pdb = PersonDB::get();

// Unclaim everything
$pcur = $pdata->get_photo($photo);
$iscur = intval($pcur->getAttribute('current'));
foreach ($pcur->getElementsByTagName('assign') as $assign) {
    $assign->parentNode->removeChild($assign);
    $pdb->dirty();
}

// Update categories
if ($iscur) {
    $cats = Categories::get();
    $cats->add_person_to('unassigned', $person);
    $cats->del_person_from('assigned', $person);
}

header("Location: viewperson.php?person={$person}");

?>
