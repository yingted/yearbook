%%
%% This is file `yearbook.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% yearbook-class.dtx  (with options: `class')
%% Copyright (C) 2002 by Julian Gilbey <jdg@debian.org>
%% All rights reserved.
%% 
%% Permission is granted to use this software and modify it for any
%% purpose.  It is distributed under the terms of the Free Software
%% Foundation's General Public License.
%% 
%% File: yearbook.dtx  Copyright (C) 2002 by Julian Gilbey <jdg@debian.org>
%% All rights reserved.
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{yearbook}[2005/05/29 v.3]
\newif\ifyb@cropmarks
\DeclareOption{cropmarks}{\yb@cropmarkstrue}
\ProcessOptions
\LoadClass{amsbook}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{pifont}
\RequirePackage{url}
\RequirePackage{graphicx}
\RequirePackage{multicol}
\RequirePackage{geometry}
\RequirePackage{paralist}
\RequirePackage{ifpdf}
\RequirePackage{verbatim}
\RequirePackage{moreverb}
\RequirePackage{multirow}
\RequirePackage{hhline}
\RequirePackage{colortbl}
\RequirePackage{array}
\RequirePackage{tikz}
\usetikzlibrary{trees}
\usetikzlibrary{folding}
\RequirePackage[romanian,greek,english]{babel}
\RequirePackage[T1]{fontenc}
\RequirePackage{verse}
\RequirePackage{fourier}
\RequirePackage{frcursive}
\RequirePackage{wasysym}
\RequirePackage{gensymb}
\RequirePackage{marvosym}
\RequirePackage{asymptote}
\RequirePackage{perpage}
\MakePerPage{footnote}

\makeindex
\geometry{%
  twoside,
  paperwidth=5.5in,
  paperheight=8.5in,
  lmargin=1.5cm,
  rmargin=1.5cm,
  tmargin=1.5cm,
  bmargin=1.5cm,
  footskip=16pt,
  headheight=12pt}
\ifyb@cropmarks\RequirePackage[frame,letter,center]{crop}\fi
\let\le=\leqslant
\let\leq=\leqslant
\let\ge=\geqslant
\let\geq=\geqslant
\newskip\yb@camperparindent \yb@camperparindent=0pt
\newskip\yb@camperparskip   \yb@camperparskip=5pt
\newdimen\yb@headerwidth  % how wide is the header on camper pages?
\yb@headerwidth=\textwidth
\newdimen\yb@biog@headwidth  \yb@biog@headwidth=2.3cm
\newdimen\yb@biog@contwidth  \yb@biog@contwidth=5.0cm
\def\yb@biog@headstyle{\bfseries}
\def\yb@biog@contstyle{}
\newdimen\yb@photo@width
\yb@photo@width=2.4cm
\newdimen\yb@posttopskip
\yb@posttopskip=0.5cm
\def\yb@photo@name@mangle#1{/home/mc2011/yb/pics/#1.jpg}
\def\PhotoNameMangle#1{\def\yb@photo@name@mangle##1{#1}}
\def\ybpics{/home/mc2011/yb/pics}
\def\suppsection{%
  \setcounter{equation}{0}%
  \@startsection{section}{1}%
  \z@{-2\linespacing\@plus-\linespacing}{\linespacing\@plus\linespacing}%
  {\normalfont\bfseries\large\centering}}
\let\section=\suppsection
\def\chapter{%
  \if@openright\cleardoublepage\else\clearpage\fi
  \thispagestyle{plain}\global\@topnum\z@
  \@afterindentfalse % this is the only change from the standard
  \secdef\@chapter\@schapter}
\def\@makechapterhead#1{\global\topskip 7.5pc\relax
  \begingroup
    \fontsize{\@xxpt}{25}\upshape
    \ifnum\c@secnumdepth>\m@ne
      \noindent\thechapter\par\fi % print the chapter number
    \noindent#1\par % and the name
  \endgroup
  \skip@=3cm \advance\skip@-\normalbaselineskip
  \vskip\skip@ }
\def\@makeschapterhead#1{\global\topskip 7.5pc\relax
  \begingroup
    \fontsize{\@xxpt}{25}\upshape
    \noindent#1\par % just print the name (starred section)
  \endgroup
  \skip@=3cm \advance\skip@-\normalbaselineskip
  \vskip\skip@ }
\def\subsection{\@startsection{subsection}{2}%
  {0pt}{.5\linespacing\@plus.7\linespacing}{-.5em}%
  {\normalfont\bfseries}}
\def\@sect#1#2#3#4#5#6[#7]#8{%
  \edef\@toclevel{\ifnum#2=\@m 0\else\number#2\fi}%
  \ifnum #2>\c@secnumdepth \let\@secnumber\@empty
  \else \@xp\let\@xp\@secnumber\csname the#1\endcsname\fi
  \@tempskipa #5\relax
  \ifnum #2>\c@secnumdepth
    \let\@svsec\@empty
  \else
    \refstepcounter{#1}%
    \edef\@secnumpunct{%
      \ifdim\@tempskipa>\z@ % not a run-in section heading
        \@ifnotempty{#8}{.\@nx\enspace}%
      \else
        \@ifempty{#8}{.}{.\@nx\enspace}%
      \fi
    }%
    \protected@edef\@svsec{%
      \ifnum#2<\@m
        \@ifundefined{#1name}{}{%
          \ignorespaces\csname #1name\endcsname\space
        }%
      \fi
      \@seccntformat{#1}%
    }%
  \fi
  \ifdim \@tempskipa>\z@ % then this is not a run-in section heading
    \begingroup #6\relax
    \@hangfrom{\hskip #3\relax\@svsec}{\interlinepenalty\@M #8\par}%
    \endgroup
  \csname #1mark\endcsname{#7}%
    \ifnum#2>\@m \else \@tocwrite{#1}{#8}\fi
  \else
  \def\@svsechd{#6\hskip #3\@svsec
    \@ifnotempty{#8}{\ignorespaces#8\unskip}%
    \ifnum#2>\@m \else \@tocwrite{#1}{#8}\fi
  }%
  \fi
  \global\@nobreaktrue
  \@xsect{#5}}
\def\theindex{\@restonecoltrue\if@twocolumn\@restonecolfalse\fi
  \columnseprule\z@ \columnsep 35\p@
  \let\@makeschapterhead\indexchap
  \@xp\chapter\@xp*\@xp{\indexname}%
  \thispagestyle{plain}%
  \let\item\@idxitem
  \parindent\z@  \parskip\z@\@plus.3\p@\relax
  \small}
\AtBeginDocument{%
  \advance\leftmargini by -\normalparindent
  \leftmargin=\leftmargini
  \labelwidth=\leftmargini \advance\labelwidth-\labelsep
  \@listi}
\newif\ifyb@camperentry
\newif\ifyb@toppart
\newif\ifyb@biogpart
\newif\ifyb@biogfield
\newif\ifyb@photopart
\newif\ifyb@mainentry
\newenvironment{camper-entry}
  {\ifyb@camperentry
    \PackageError{yearbook}{nested camper-entry environment}{The
     camper-entry environment can only appear^^Jas a top-level
     environment}
   \fi
   \clearpage\yb@camperentrytrue
   \parindent=0pt
   \parskip=0pt
  }
  {\newpage}
\def\indexentry#1{\index{#1}}
\newbox\yb@biog@box
\newbox\yb@photo@box
\newif\ifyb@photo@overlays
\yb@photo@overlaysfalse
\newenvironment{top-part}
  {\ifyb@camperentry
    \ifyb@toppart
      \PackageError{yearbook}{nested top-part environment}{The
       top-part environment can only appear^^Jdirectly inside a
       camper-entry environment}
    \else
     \ifyb@mainentry
      \PackageError{yearbook}{top-part environment in main-entry}
       {The top-part environment can only appear^^Jdirectly inside a
       camper-entry environment}
     \else
      \yb@topparttrue
     \fi
    \fi
   \else\PackageError{yearbook}{lonely top-part environment}{The
     top-part environment can only appear^^Jwithin a camper-entry
     environment}
   \fi
   \frenchspacing
  }
  {\ifyb@photo@overlays
    \hbox to \textwidth{%
     \hbox to \yb@headerwidth{%
      \ifvoid\yb@biog@box
       \PackageWarning{yearbook}{There is no biographical-details
        content for this person}%
       \hbox{\small No biog details yet!!}%
      \else
       \vtop{\vskip\z@ \box\yb@biog@box}
       \global\setbox\yb@biog@box\box\voidb@x
      \fi
      \hfil
      \ifdim\wd\yb@photo@box=0pt
       \PackageWarning{yearbook}{There is no photo for this person}%
       \hbox{\small No photo yet!!}%
      \else
       \vtop{\vskip\z@ \hbox to \yb@photo@width{\hss\box\yb@photo@box}}
       \global\setbox\yb@photo@box\box\voidb@x
      \fi
     }%
     \hss}
   \else % \yb@photo@overlaysfalse
    \hbox to \textwidth{%
     \hbox to \z@{%
      \hbox to \yb@headerwidth{%
       \hfil
       \ifdim\wd\yb@photo@box=0pt
        \PackageWarning{yearbook}{There is no photo for this person}%
        \hbox{\small No photo yet!!}%
       \else
        \vtop{\vskip\z@ \box\yb@photo@box}%
        \global\setbox\yb@photo@box\box\voidb@x
       \fi
      }%
      \hss}%
     \hbox{%
      \ifvoid\yb@biog@box
       \PackageWarning{yearbook}{There is no biographical-details
        content for this person}%
       \hbox{\small No biog details yet!!}%
      \else
       \vtop{\vskip\z@ \box\yb@biog@box}%
       \global\setbox\yb@biog@box\box\voidb@x
      \fi}%
     \hss
    }%
   \fi
   \vspace{\yb@posttopskip}
  }
\newbox\yb@name@box
\newbox\yb@address@box
\newbox\yb@phonenum@box
\newbox\yb@msn@box
\newbox\yb@livejournal@box
\newbox\yb@emails@box
\newbox\yb@aims@box
\newbox\yb@icqs@box
\newbox\yb@extrabiog@box
\newbox\yb@birthday@box
\newbox\yb@school@box
\newbox\yb@tempbox
\newenvironment{biographical-details}
  {\ifyb@toppart
    \ifyb@biogpart
     \PackageError{yearbook}{nested biographical-details environment}
       {The biographical-details environment can only
        appear^^Jdirectly inside a top-part environment}
    \else
     \ifyb@photopart
      \PackageError{yearbook}{biographical-details can't appear in photo}
        {The biographical-details environment can only
         appear^^Jdirectly inside a top-part environment}
     \else
      \yb@biogparttrue
      % Handle the address environment definition (see below)
      \let\address\camperaddress
      \let\endaddress\endcamperaddress
     \fi
    \fi
   \else
    \PackageError{yearbook}{missing top-part environment}{The
     biographical-details environment can only appear^^Jdirectly
     inside a top-part environment}
   \fi}
  {\global\setbox\yb@biog@box=\vbox{
    \ifvoid\yb@name@box\else
      \box\yb@name@box \global\setbox\yb@name@box\box\voidb@x\fi
    \ifvoid\yb@address@box\else
      \box\yb@address@box \global\setbox\yb@address@box\box\voidb@x\fi
    \ifvoid\yb@phonenum@box\else
      \box\yb@phonenum@box \global\setbox\yb@phonenum@box\box\voidb@x\fi
    \ifvoid\yb@msn@box\else
      \box\yb@msn@box \global\setbox\yb@msn@box\box\voidb@x\fi
    \ifvoid\yb@livejournal@box\else
      \box\yb@livejournal@box \global\setbox\yb@livejournal@box\box\voidb@x\fi
    \ifvoid\yb@emails@box\else
      \box\yb@emails@box \global\setbox\yb@emails@box\box\voidb@x\fi
    \ifvoid\yb@aims@box\else
      \box\yb@aims@box \global\setbox\yb@aims@box\box\voidb@x\fi
    \ifvoid\yb@icqs@box\else
      \box\yb@icqs@box \global\setbox\yb@icqs@box\box\voidb@x\fi
    \ifvoid\yb@extrabiog@box\else
      \box\yb@extrabiog@box \global\setbox\yb@extrabiog@box\box\voidb@x\fi
    \ifvoid\yb@birthday@box\else
      \box\yb@birthday@box \global\setbox\yb@birthday@box\box\voidb@x\fi
    \ifvoid\yb@school@box\else
      \box\yb@school@box \global\setbox\yb@school@box\box\voidb@x\fi
   }}
\newif\ifyb@biogfield@widelabel
\newenvironment{yb@biogfield}[2]
  {\ifyb@biogpart
    \ifyb@biogfield
     \PackageError{yearbook}{nested biographical details environments}
       {The biographical details environments can only
        appear^^Jdirectly inside a biographical-details environment}
    \else
     \yb@biogfieldtrue
    \fi
   \else
    \PackageError{yearbook}{missing biographical-details environment}
     {The biographical details environments can only appear^^Jdirectly
     inside a biographical-details environment}
   \fi
   \setbox\@tempboxa=\hbox{\normalfont\yb@biog@headstyle #1\hskip0.5ex}%
   \ifdim\wd\@tempboxa>\yb@biog@headwidth
    \yb@biogfield@widelabeltrue\else\yb@biogfield@widelabelfalse\fi
   \global\setbox#2=\hbox\bgroup
    \hbox to \yb@biog@headwidth{\normalfont\yb@biog@headstyle #1\strut\hss}%
    \begin{minipage}[t]{\yb@biog@contwidth}%
     \ifyb@biogfield@widelabel\leavevmode\\\fi
     \everypar{\hangindent=1em \hangafter=1 }%
     \def\\{\ifhmode\strut\fi\@centercr}%
     \rightskip=0pt plus 2cm\relax
     \normalfont\yb@biog@contstyle
     \ignorespaces
  }
  {\ifhmode\unskip\strut\fi
   \end{minipage}\egroup}
\newenvironment{name}
  {\begin{yb@biogfield}{Name}{\yb@name@box}}
  {\end{yb@biogfield}}
{\catcode`\^^M=13 %
 \gdef\yb@obeylines{\catcode`\^^M13 \def^^M{\ifhmode\strut\fi\\}\@gobblecr}}%
\newenvironment{camperaddress}
  {\begin{yb@biogfield}{Address}{\yb@address@box}%
    \let\\=\@centercr \yb@obeylines
  }
  {\end{yb@biogfield}}
\newenvironment{phonenum}
  {\begin{yb@biogfield}{Phone}{\yb@phonenum@box}}
  {\end{yb@biogfield}}
\newenvironment{msn}
  {\begin{yb@biogfield}{MSN}{\yb@msn@box}}
  {\end{yb@biogfield}}
\newenvironment{livejournal}
  {\begin{yb@biogfield}{Livejournal}{\yb@livejournal@box}}
  {\end{yb@biogfield}}
\newenvironment{emails}
  {\begin{yb@biogfield}{Email}{\yb@emails@box}%
   \let\email\camperemail}
  {\end{yb@biogfield}}
\def\camperemail{\leavevmode\begingroup \urlstyle{tt}\Url}
\newenvironment{aims}
  {\begin{yb@biogfield}{AIM}{\yb@aims@box}}
  {\end{yb@biogfield}}
\def\aim{\leavevmode\begingroup \urlstyle{rm}\Url}
\newenvironment{icqs}
  {\begin{yb@biogfield}{ICQ}{\yb@icqs@box}}
  {\end{yb@biogfield}}
\def\icq{\leavevmode\begingroup \urlstyle{rm}\Url}
\newenvironment{birthday}
  {\begin{yb@biogfield}{Birthday}{\yb@birthday@box}}
  {\end{yb@biogfield}}
\newenvironment{school}
  {\begin{yb@biogfield}{School}{\yb@school@box}}
  {\end{yb@biogfield}}
\newenvironment{extrabiog}[1]
  {\begin{yb@biogfield}{#1}{\yb@tempbox}}
  {\end{yb@biogfield}%
   \ifvoid\yb@extrabiog@box
    \global\setbox\yb@extrabiog@box\vbox{\box\yb@tempbox}
   \else
    \global\setbox\yb@extrabiog@box\vbox{\unvbox\yb@extrabiog@box\box\yb@tempbox}
   \fi
   \global\setbox\yb@tempbox\box\voidb@x
  }
\newenvironment{photo}
  {\ifyb@toppart
    \ifyb@photopart
     \PackageError{yearbook}{nested photo environment}
       {The photo environment can only appear directly inside^^Ja
        top-part environment}
    \else
     \ifyb@biogpart
      \PackageError{yearbook}{photo can't appear in biographical-details}
        {The photo environment can only appear directly inside^^Ja
         top-part environment}
     \else
      \yb@photoparttrue
     \fi
    \fi
   \else
    \PackageError{yearbook}{missing top-part environment}
      {The photo environment can only appear directly inside^^Ja
       top-part environment}
   \fi
   \global\setbox\yb@photo@box=\hbox\bgroup\ignorespaces
  }
  {\egroup}
\ifpdf\def\yb@photo@ext{jpg}\else\def\yb@photo@ext{eps}\fi
\def\includephoto{\@testopt\yb@includephoto{}}
\def\yb@includephoto[#1]#2{%
  \def\yb@photo@opts{#1}%
  \IfFileExists{\yb@photo@name@mangle{#2}.\yb@photo@ext}%
   {\edef\yb@photo@use{\yb@photo@name@mangle{#2}}}%
   {\edef\yb@photo@use{\yb@photo@name@mangle{nopicture}}}%
  \ifx\yb@photo@opts\empty
    \includegraphics[width=\yb@photo@width,keepaspectratio]{\yb@photo@use}%
  \else
    \edef\yb@photo@opts{width=\yb@photo@width,keepaspectratio,\yb@photo@opts}%
    \expandafter\includegraphics\expandafter[\yb@photo@opts]{\yb@photo@use}%
  \fi
}
\newenvironment{main-entry}
  {\ifyb@camperentry
    \ifyb@mainentry
      \PackageError{yearbook}{nested main-entry environment}{The
       main-entry environment can only appear^^Jdirectly inside a
       camper-entry environment}
    \else
     \ifyb@toppart
      \PackageError{yearbook}{main-entry environment in top-part}
       {The main-entry environment can only appear^^Jdirectly inside a
       camper-entry environment}
     \else
      \yb@mainentrytrue
     \fi
    \fi
   \else\PackageError{yearbook}{lonely main-entry environment}{The
     main-entry environment can only appear^^Jwithin a camper-entry
     environment}
   \fi
   \parindent=\yb@camperparindent
   \parskip=\yb@camperparskip
  }
  {\par}
\newenvironment{entry-enumerate}{%
  \ifnum \@enumdepth >3 \@toodeep\else
      \advance\@enumdepth \@ne
      \edef\@enumctr{enum\romannumeral\the\@enumdepth}\list
      {\csname label\@enumctr\endcsname}{%
        \leftmargin=18pt\raggedright\usecounter
        {\@enumctr}\def\makelabel##1{\hss\llap{\upshape##1}}}\fi
}{%
  \endlist
}
\newenvironment{entry-itemize}{%
  \ifnum\@itemdepth>3 \@toodeep
  \else \advance\@itemdepth\@ne
    \edef\@itemitem{labelitem\romannumeral\the\@itemdepth}%
    \list{\csname\@itemitem\endcsname}{%
       \leftmargin=18pt\raggedright
       \def\makelabel##1{\hss\llap{\upshape##1}}}%
  \fi
}{%
  \endlist
}

\newenvironment{quoteslist}[1][\proofname]{\par
  \pushQED{\qed}%
  \normalfont \topsep6\p@\@plus6\p@\relax
  \begin{list}{}{%
    \itemindent=0pt
    \leftmargin=0pt
    \labelwidth=0pt
    \parsep=0pt
    \listparindent=0pt
    \itemsep=6pt plus 1pt minus 1pt }
    \item[\hskip\labelsep
          \scshape
      #1\@addpunct{.}]\ignorespaces
}{%
  \popQED\end{list}\@endpefalse
}

\def\yb@colon{:}
\newcommand{\quoteslabel}[1]{%
  \def\@quoter{#1}%
  \ifx\@quoter\yb@colon \else
    \hspace\labelsep \normalfont\scshape #1\fi}
\newcommand{\saidby}[1]{%
 \@ifnextchar[{\saidbycontext{#1}}{\saidbynocontext{#1}}}
\def\saidbynocontext#1{\item[{#1:}]}
\def\saidbycontext#1[#2]{\item[{#1}] (#2):}
\newcommand{\saidbync}[1]{\item[{#1}]}
\newenvironment{ybquote}{\list{}{%
  \itemindent-\leftmargini
  \labelwidth\z@ \let\makelabel\quoteslabel}%
}{
  \endlist
}
\newenvironment{ybquotes}{
  \let\quote=\ybquote \let\endquote=\endybquote
}{}

\def\yb@sep{:}
\def\yb@namestyle{\scshape}
\newcommand{\quotelabel}[1]{%
  \def\@quoter{#1}%
  \ifx\@quoter\yb@sep \else
    \hspace\labelsep \normalfont {\yb@namestyle #1}\fi}
\newcommand{\speaker}[1]{%
  \@ifnextchar[{\speakercontext{#1}}{\speakernocontext{#1}}}
\def\speakernocontext#1{\item[{#1\yb@sep}]}
\def\speakercontext#1[#2]{\item[{#1}] (#2)\yb@sep\hskip\labelsep}
\newcommand{\speakernc}[1]{\item[{#1}]}
\newenvironment{ybqwote}{\noindent\begin{minipage}{\textwidth}\list{}{%
  \itemindent-\leftmargini
  \labelwidth\z@ \let\makelabel\quotelabel}%
}{
  \endlist
  \end{minipage}
  \par
  \vspace{\baselineskip}
}
\newenvironment{ybquotes2}{
  %\addtolength{\parskip}{\normalbaselineskip}
  \let\quote=\ybqwote \let\endquote=\endybqwote
}{}

\newenvironment{recommendations}{\begin{itemize}}{\end{itemize}}
\newcommand{\recommend}[3]{%
  \begingroup
  \def\@author{#1}%
  \def\@title{#2}%
  \def\@comment{#3}%
  \def\dospace{}
  \def\dodotspace{\ifnum\spacefactor>1000 \else.\fi
    \def\dospace{\space\def\dospace{}}}
  \item
     \ifx\@author\empty\else{#1}\dodotspace\fi
     \ifx\@title\empty\else\dospace\textbf{#2}\dodotspace\fi
     \ifx\@comment\empty\else\dospace\textit{#3}\dodotspace\fi
  \endgroup
}

\newcommand{\superlativeslabel}[1]{\hspace\labelsep \upshape\bfseries #1:}
\newenvironment{superlatives}{\list{}{%
  \itemindent-\leftmargini
  \labelwidth\z@ \let\makelabel\superlativeslabel}%
}{
  \endlist
}
\newcommand{\fullpagegraphics}[2][5.5in]{
  \thispagestyle{empty}%
   \vspace*{0pt plus 1fil minus 100pt}%
   \vskip -\headheight
    \hbox to \hsize{\hss
    \includegraphics[width=#1,keepaspectratio]{#2}\hss}%
   \nobreak\vspace{0pt plus 1fil minus 100pt}
  \newpage}
\newlength{\strikewidth}
\newlength{\strikelength}
\setlength{\strikewidth}{.5pt}

\newcommand{\strikethrough}[1]{%
 \settowidth{\strikelength}{#1}%
 \hbox{#1\/\hspace{-\strikelength}%
  \rule[0.5ex]{\strikelength}{\strikewidth}}}

%BEGIN HERMAN EDIT
\def\subsubsection{\@startsection{subsubsection}{2}%
  {0pt}{.5\linespacing\@plus.7\linespacing}{-.5em}%
  {\normalfont\itshape}}
%END HERMAN EDIT


\InputIfFileExists{yearbook.cfg}{}{}
\endinput
%%
%% End of file `yearbook.cls'.
