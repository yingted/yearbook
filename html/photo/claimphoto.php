<?php

// vim: set sw=4 sts=4:

require_once('photolib.php');

checkperms();
$loggedin or die('Not logged in.');

$person = input_unescape_slashes($_POST['person']);
$name = input_unescape_slashes($_POST['name']);
$photo = input_unescape_slashes($_POST['photo']);
$viewall = intval($_POST['viewall']);

(strlen($name) > 1) or die('Please enter a name.');

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

//$pcur = $pdata->get_photo_current();
$pcur = $pdata->get_photo($photo);
$iscur = intval($pcur->getAttribute('current'));
if ($pcur->getElementsByTagName('assign')->length) {
    die('Photo already claimed.');
}

assert(!isset($pdata->categories['assigned']));

$pdb = PersonDB::get();
$cats = Categories::get();
$node = $pcur->ownerDocument->createElement('assign');
$node->appendChild($node->ownerDocument->createTextNode($name));
$node->setAttribute('time', time());
$pcur->appendChild($node);
$pdb->dirty();

if ($iscur) {
    $cats->del_person_from('unassigned', $person);
    $cats->add_person_to('assigned', $person);
}

header("Location: viewperson.php?viewall={$viewall}&person={$person}");

?>
