<?php

require_once('photolib.php');

checkperms();

?>
<html>
<head>
<title>Editors</title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<p><a href="index.php">Back to photos</a></p>
<?php
if($loggedin)
{
    ?>
<h1>Editors</h1>

<?
    $db = PersonDB::get();
    $categories = Categories::get();

    echo "<h2>Unassigned People</h2>\n";
    $categories->display_blocks_by_group('unassigned', 'edit');

    echo "<h2>Assigned People</h2>\n";
    $categories->display_blocks_by_group('assigned', 'edit');

    echo "<h2>Edits Needing Okays</h2>\n";
    $categories->display_blocks_by_group('needokay', 'needokay');

?>

<?php
} else {
?>
    <h1>Not logged in</h1>
    <p>You are not logged in. If you are on photo staff, enter a photo staff
    password to log in. Otherwise, you shouldn't need to see this page.</p>

    <form method="post" action=".">
    <label>Password: <input type="password" name="pass" /></label>
    </form>
<?php
}
?>
</body>
</html>
