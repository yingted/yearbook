#! /usr/bin/perl -w

use XML::Simple;

sub xmltree_stripspaces ($);

my $ybhome = '@@YBHOMEDIR@@';
my $xmldir = "$ybhome/pages/entries/";
chdir $xmldir or die "Couldn't chdir $xmldir: $!\n";

my $force = 0;
my $quiet = 0;

if (@ARGV == 0) {
    die "Usage: $0 [--force|-q] name\n";
}

if ($ARGV[0] eq '--force') {
    $force = 1;
    shift;
} elsif ($ARGV[0] eq '-q') {
    $quiet = 1;
    shift;
}

if (@ARGV != 1) {
    die "Usage: $0 [--force|-q] name\n";
}

umask 002;  # files must be group writeable

my $name = shift;
$name =~ s/\.xml$//;
my $xmlfile = "$name.xml";
my $texfile = "$name.tex";
if (! -r $xmlfile) {
    die "Can't find $name.xml!\n";
}

my $data = XMLin($xmlfile,
		 ForceArray => [qw(email aim icq extrabiog)],
		 KeyAttr => { 'extrabiog' => 'fieldname' },
		 SuppressEmpty => '',
		 );
xmltree_stripspaces($data);
my $version = $data->{'version'};
if ($version ne 'final' and $version ne 'draft') {
    if ($force) {
	warn "Version of $name.xml is $version, but proceeding anyway as requested.\n";
    } else {
	if ($quiet and -M $texfile < -M $xmlfile) {
	    exit 0;
	} else {
	    die "Version of $name.xml is $version, aborting.  (Use --force to force.)\n";
	}
    }
}

open TEX, ">$texfile" or die "Can't open $texfile for writing: $!\n";

print TEX <<"EOT";
% version=$data->{'version'}
\\begin{camper-entry}
\\indexentry{$data->{'indexentry'}}
  \\begin{top-part}
    \\begin{biographical-details}
EOT

my $biog = $data->{'top-part'}->{'biographical-details'};
if ($biog->{'name'} ne $biog->{'registeredname'}) {
    print TEX "      %%% Registered name: $biog->{'registeredname'}\n";
}
for my $field (qw(name address phone)) {
    if (exists $biog->{$field}) {
	print TEX "      \\begin{$field}$biog->{$field}\\end{$field}\n";
    }
}
for my $field (qw(email aim icq)) {
    if (exists $biog->{"${field}s"}) {
	print TEX "      \\begin{${field}s}\n";
	print TEX "        ", join(", ",
		       map { "\\${field}{$_}"; }
		       @{$biog->{"${field}s"}{$field}});
	print TEX "\n      \\end{${field}s}\n";
    }
}
for my $field (qw(msn livejournal)) {
    if (exists $biog->{$field}) {
        my $text = $biog->{$field};
        $text =~ s/_/\\_/g; # escape
	print TEX "      \\begin{$field}$text\\end{$field}\n";
    }
}
# XML::Simple calls the content of an element 'content'
if (exists $biog->{'extrabiog'}) {
    while (my ($f, $v) = each %{$biog->{'extrabiog'}}) {
	print TEX "      \\begin{extrabiog}{$f}$v->{'content'}\\end{extrabiog}\n";
    }
}
for my $field (qw(birthday school)) {
    if (exists $biog->{$field}) {
	print TEX "      \\begin{$field}$biog->{$field}\\end{$field}\n";
    }
}

print TEX <<"EOT";
    \\end{biographical-details}
    \\begin{photo}
EOT

if (exists $data->{'photo'}->{'options'}) {
    print TEX "      \\includephoto[$data->{'photo'}->{'options'}]{$data->{'photo'}->{'filename'}}\n";
} else {
    print TEX "      \\includephoto{$data->{'photo'}->{'filename'}}\n";
}

print TEX <<"EOT";
    \\end{photo}
  \\end{top-part}
  \\begin{main-entry}
$data->{'main-entry'}
  \\end{main-entry}
\\end{camper-entry}
EOT

if (exists $data->{'comment'}) {
    my $comment = $data->{'comment'};
    $comment =~ s/^/% /gm;
    print TEX "% Comments:\n$comment\n";
}

close TEX or warn "Problems closing $texfile: $!\n";

exit 0;



sub xmltree_stripspaces ($)
{
    my $head = $_[0];

    if (ref $head eq 'ARRAY') {
	foreach my $elt (@$head) {
	    xmltree_stripspaces $elt;
	}
    } else {
	map { if (ref $$head{$_}) { xmltree_stripspaces $$head{$_}; }
	      else { $$head{$_} =~ s/^\s+//; $$head{$_} =~ s/\s+$//; } }
	    keys %$head;
    }
}
