<?php

require_once('photolib.php');

checkperms();

?>
<?php
if($loggedin)
{
    ?>
<html>
<head>
<title>Everyone</title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<p><a href="index.php">Back to photos</a></p>
<h2>Everyone</h2>
<?
foreach (array_keys($people) as $person) {
    $pdata = new Person($person);
    echo "<a class=\"nounderline\" href=\"viewperson.php?person={$person}\">\n";
    $pdata->print_data('edit');
    echo "</a>\n";
}
?>
</body>
</html>
<?php
} else {
    draw_stupid_login_form();
}
?>
