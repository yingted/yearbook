<?php

// vim: se sw=4 sts=4:

require_once('photolib.php');

checkperms();

$person = $_GET['person'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$pdata = new Person($person);
$locked = $pdata->is_locked();
$ybphoto = $pdata->get_yb_photo();

$viewall = intval($_GET['viewall']);

function draw_uploadedit_form($person, $photo, $editor, $editscount) {
    $person_escaped = htmlspecialchars($person);
    $photo_escaped = htmlspecialchars($photo);
    $editor_escaped = htmlspecialchars($editor);
    echo "<form method=\"post\" action=\"uploadedit.php\" enctype=\"multipart/form-data\">\n";
    echo "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1100000\" />\n";
    echo "<input type=\"hidden\" name=\"person\" value=\"$person_escaped\" />\n";
    echo "<input type=\"hidden\" name=\"photo\" value=\"$photo_escaped\" />\n";
    echo "<input type=\"hidden\" name=\"editor\" value=\"$editor_escaped\" />\n";
    echo "<p><b>Upload edit:</b> <input type=\"file\" name=\"photo\" />\n";
    echo "<input type=\"submit\" name=\"sub\" value=\"Upload\" /></p>\n";
    echo "</form>\n";

    if ($editscount == 0) {
        echo "<form method=\"post\" action=\"acceptasedit.php\" enctype=\"multipart/form-data\">\n";
        echo "<input type=\"hidden\" name=\"person\" value=\"$person_escaped\" />\n";
        echo "<input type=\"hidden\" name=\"photo\" value=\"$photo_escaped\" />\n";
        echo "<input type=\"hidden\" name=\"editor\" value=\"$editor_escaped\" />\n";
        echo "<p><b>Accept photo as edited:</b>\n";
        echo "<input type=\"submit\" name=\"sub\" value=\"Accept\" /></p>\n";
        echo "</form>\n";
    }
}

function display_photo($pic) {
    global $person;
    global $locked;
    global $isadmin;
    global $viewall;
    global $ybphoto;

    $filename = $pic->getAttribute('filename');
    $current = intval($pic->getAttribute('current'));
    if ($current)
        echo '<a name="current"></a>';
    echo "<div class=\"picentry";
    if ($current) { echo ' piccurrent'; }
    echo "\">\n";

    if ($isadmin)
        echo "<div style=\"float:right; margin: 0 5px 0 5px;\"><a href=\"delphoto.php?person=$person&amp;photo={$filename}\">Delete photo</a></div>\n";
    if (!$current)
        echo "<div style=\"float:right; margin: 0 5px 0 5px;\"><a href=\"setcurrent.php?person=$person&amp;photo={$filename}\">Set current</a></div>\n";


    echo "<a class=\"nounderline\" href=\"img/{$filename}.jpg\" />\n";
    echo "<img class=\"origthumb\" src=\"img/thumb/{$filename}.jpg\" />\n";
    echo "</a>\n";

    $edits = $pic->getElementsByTagName('edit');
    $editscount = 0;
    foreach ($edits as $edit) {
        $editscount++;
        $status = $edit->getAttribute('status');
        echo "<div class=\"picedit";
        if ($status == -1) {
            echo ' ereject';
        } else if ($status == 1) {
            echo ' eaccept';
        }
        echo "\">\n";

        $efilename = $edit->textContent;

        if ($isadmin) {
            echo "<div class=\"adminlink\"><a href=\"moderate.php?person={$person}&amp;photo={$filename}&amp;edit={$efilename}\">Moderate</a></div>";
            if ($status == 1) {
                if ($efilename == $ybphoto) {
                    echo "<div class=\"pcomment\">[Uploaded to yearbook]</div>\n";
                } else {
                    echo "<div class=\"adminlink\"><a href=\"pushtoyb.php?person={$person}&amp;photo={$filename}&amp;edit={$efilename}\">Push to yearbook</a></div>";
                }
            }
            echo "<div class=\"adminlink\"><a href=\"deleteedit.php?person={$person}&amp;photo={$filename}&amp;edit={$efilename}\">Delete</a></div>";
        }


        echo "<a class=\"nounderline\" href=\"img/{$efilename}.jpg\" />\n";
        echo "<img class=\"editthumb\" src=\"img/thumb/{$efilename}.jpg\" />\n";
        echo "</a>\n";
        echo "By {$edit->getAttribute('editor')}";

        echo "</div>\n";
    }

    if (true || $isadmin || $current) {
        $assign = $pic->getElementsByTagName('assign')->item(0);
    $photo_filename = $pic->getAttribute('filename');
        if ($assign) {
            echo '<div class="passign">Claimed by ';
        echo htmlspecialchars($assign->textContent);
            echo ' at ';
            echo date("Y-m-d g:i a", $assign->getAttribute('time'));
            if (!$locked) {
        echo " <a href=\"cancelclaim.php?person={$person}&amp;photo={$photo_filename}\">(Remove claim)</a>";
                //echo "<br /><a href=\"editclaim.php?person={$person}&amp;photo={$photo_filename}\">Edit claim</a>";
        draw_uploadedit_form($person, $photo_filename, $assign->textContent, $editscount);
            }
            echo "</div>";
        } else if (!$locked) {
            echo '<div class="pclaim">';
            echo "<b>Claim this photo</b> <form method=\"post\" action=\"claimphoto.php\"><input name=\"person\" type=\"hidden\" value=\"{$person}\" />";
            echo "<input name=\"photo\" value=\"{$filename}\" type=\"hidden\" />";
            echo "<input name=\"viewall\" value=\"{$viewall}\" type=\"hidden\" />";
            echo 'Name: <input name="name" /> <input name="sub" type="submit" value="Claim" /></form>';
            echo '</div>';
        }
    }

    echo "</div>\n\n";
}
?>
<?php
if($loggedin)
{
?>
<html>
<head>
<title><? echo $people[$person] ?></title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<?php
    if ($locked) {
        echo '<body class="lockedentry">';
    } else {
        echo '<body>';
    }
?>
<p><a href="index.php">Back to photos</a></p>
<h1><? echo $people[$person] ?></h1>
<?
    if ($locked) {
        echo '<img src="img/lock.png" />';
        echo '<i>Entry is locked.</i>';
    }

    if ($isadmin) {
        echo "<a href=\"resetcategories.php?person={$person}\">Reset categories</a>\n";
    }

    if ($complain = $pdata->get_complaint()) {
        echo "<div class=\"pcomplaint\">$complain <div class=\"pcclear\"><a href=\"clearcomplain.php?person={$person}\">Clear complaint</a></div></div>";
    }

    $pictures = $pdata->get_photos();
    $pcount = count($pictures);
    if ($viewall) {
        echo "<h2>All Photos</h2>\n";
        if ($pcount) {
            echo "<p><a href=\"#current\">Go to current</a> | <a href=\"viewperson.php?person={$person}&amp;viewall=0\">Show only current</a></p>";
            foreach ($pictures as $pic) {
                display_photo($pic);
            }
        } else {
            echo "<i>No pictures</i>";
        }
    } else {
        $picture = $pdata->get_photo_current();
        if ($picture) {
            if ($pcount > 1) {
                echo '<h2>Current Photo</h2>';
                echo "<p><a href=\"viewperson.php?person={$person}&amp;viewall=1\">Show all pictures ($pcount total)</a></p>";
	    } else {
                echo '<h2>Current (and only) Photos</h2>';
	    }
            display_photo($picture);
        } else {
            echo "<i>No pictures</i>";
        }
    }

    if (!$locked) {
    if ($pcount)
        echo '<h2 onclick="show_hide_upload_form()" style="cursor:pointer;">Upload Replacement Source Photo</h2>';
    else
        echo '<h2>Upload Source Photo</h2>';
?>
<div id="uneditedupload">
<p><i>Add a new source picture to edit.</i></p>
<form method="post" action="upload.php" enctype="multipart/form-data">
<input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
<input type="hidden" name="person" value="<? echo $person ?>" />
<input type="file" name="photo" />
<input type="submit" name="sub" value="Upload" />
</form>
</div>
<script type="text/javascript">
<!--
var visible = true;
function show_hide_upload_form() {
    if (visible) {
    document.getElementById("uneditedupload").style.display = "none";
    visible = false;
    } else {
    document.getElementById("uneditedupload").style.display = "block";
    visible = true;
    }
}
<?
if ($pcount > 0)
    echo "show_hide_upload_form();\n";
?>
//-->
</script>
</body>
</html>
<?php
    }
} else {
    draw_stupid_login_form();
}
?>
