<?php
require_once('../config.php');
require_once('lib/quote.php');
$loggedin = require_priv('quote');
?>
<html>
<head>
<title>Quotes Admin</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>

Click <a href="../">here</a> to return to the main yearbook page.

<p>Click <a href="index.php">here</a> to return to the main admin page.


<?php


// Determine the what to do
$actions = array('edit', 'list', 'cat-edit');
$action = $_REQUEST['action'];
if(!in_array($action, $actions)) $action = 'list';

$quotes = new Quote();
$quotes->maybe_galley();

// Do the action
if($action == 'list')
{
    print "<h1>Quote List</h1>";
    print $quotes->get_list();
} else if($action == 'edit') {
    $id = intval($_REQUEST['id']);
    print "<h1>Editing Quote #$id</h1>\n";
    $quotes->do_edit_form($id);
} else if($action == 'cat-edit') {
    //print_r($_REQUEST);
    $name = clean_space($_REQUEST['name']);
    $oldname = clean_space($_REQUEST['oldname'] ? 
        $_REQUEST['oldname'] : $_REQUEST['name']);
    print "<h1>Editing Quote Category $name</h1>\n";
    $quotes->do_cat_edit_form($oldname);
}
?>
</body>
</html>
