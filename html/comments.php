<?php
require_once('config.php');
require_once('year_lib.php');
umask(002);

print start_html("Comments");
?>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>Comments</h1>
<?php
if ($_REQUEST['action'] === 'submitted') {
  foreach ($_REQUEST as $varname => $varval) {
    $_REQUEST[$varname]=stripslashes($_REQUEST[$varname]);
  }

  $lock=fopen("$pagesprefix/comments.lock","r");
  if (!flock($lock,LOCK_EX)) {
    error("Someone seems to have submitted an entry at the same time as you.\nPlease try reloading the page; if you still see this message, find a member of the yearbook staff.");
  }

  $commentlist = fopen("$pagesprefix/comments.txt", "ab+");
  if (!$commentlist) {
    error("I can't open the comments file. This is bad.\nFind a member of the yearbook staff as soon as possible and do anything you can to get them to fix it!");
  }

  print "<p>Thank you for submitting the following comment.  If there are any
    problems, please speak to one of the yearbook staff!</p>\n";

  $submitter = $_REQUEST['submitter'];
  $comment = $_REQUEST['comment'];
  if ($comment !== "") {
    fwrite($commentlist, "$comment\n");
    print "<font size=\"-1\">\n";
    print "<p>$comment</p>\n";
    fwrite($commentlist, "---Submitted by ".$submitter.", ");
    fwrite($commentlist, date("r"));
    fwrite($commentlist, "\n\n");
    print "<p>Submitted by: $submitter</p>\n";
    print "</font>\n";
    print "<br/><p>Comment submitted! Want to <a href=\"{$_SERVER['PHP_SELF']}\">submit another</a>?</p>";
    fclose($commentlist);
    flock($lock,LOCK_UN);
    fclose($lock);
    system("$utilprefix/cvs $pagesprefix/comments.txt");
  } else
   print "<p>No comment was submitted.  Want to <a href=\"{$_SERVER['PHP_SELF']}\">try again</a>?</p>";
} else {
?>

<h2>Instructions</h2>
<p>Please submit one comment at a time!</p>
<p>If you don't feel like typing, either write down your comments and put
them in the box at the notice board, or speak to one of the yearbook staff.</p>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>"
      enctype="multipart/form-data"
      method="post">

<h2>The Comment</h2>
<textarea name="comment" rows="8" cols="40"></textarea>
<br /> 
<p>Your Name: <input type="text" name="submitter" /></p>
<p>
<input type="hidden" name="_charset_" />
<input type="hidden" name="action" value="submitted" />
<input type="submit" name="submit" value="submit me!" />
</p>
</form>
<?php
}
?>

<p>Return to the <a href="index.php">main yearbook page</a></p>
</body>
</html>
