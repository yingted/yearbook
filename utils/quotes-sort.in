#! /usr/bin/perl -w

use File::Copy;
use FileHandle;
use Fcntl ':flock'; # import LOCK_* constants

my $ybhome = '@@YBHOMEDIR@@';

chdir "$ybhome/pages" or die "Can't chdir $ybhome/pages: $!\n";

# keypress, label, file
my %sects = ( '1' => [ 'games ', 'ttgames' ],
	      '2' => [ 'comp  ', 'ttcomputers' ],
	      '3' => [ 'destr ', 'ttdestroy' ],
	      '4' => [ 'random', 'ttrandom' ],
	      '5' => [ 'cultur', 'ttculture' ],
	      '6' => [ 'cheapm', 'ttcheapmath' ],
	      '7' => [ 'crazym', 'ttcrazymath' ],
	      '8' => [ 'puns  ', 'ttpuns' ],
	      '9' => [ 'food  ', 'ttfood' ],
	      '0' => [ 'realty', 'ttreality' ],
	      'a' => [ 'announ', 'ttannounce' ],
	      'b' => [ 'people', 'ttpeople' ],
              );
my @sectkeys = qw(1 2 3 4 5 6 7 8 9 0 a b);
# or if they're all numerical and you want them in order, simply:
# my @sectkeys = sort { $a <=> $b } keys %sects;
my %keyshash = map { $_ => 1 } @sectkeys;

my $quotesfile;
if (@ARGV > 1) {
    die "Usage: $0 [quotes file]\n";
} elsif (@ARGV == 1) {
    $quotesfile = shift;
} else {
    open LOCK, "quotes.lock"
	or die "Couldn't open quotes.lock lockfile: $!\n";
    flock(LOCK, LOCK_EX)
	or die "Couldn't get lock on quotes.lock lockfile: try again, and if you still\nsee this message, ask Julian\n";

    my @recdone = sort { $a <=> $b }
                      map { s/^quotes-processed-//; s/\.tex$//; $_ }
                      glob ("quotes-processed-*.tex");
    unshift @recdone, 0;  # avoid warnings
    my $count = $recdone[-1] + 1;
    $quotesfile = "quotes-processed-$count.tex";

    copy "quotes-submitted.tex", $quotesfile
	or die "Couldn't copy quotes-submitted.tex: $!\n";

    # Wipe the old contents of the quotes file
    open CLEAR, ">quotes-submitted.tex";
    close CLEAR;

    flock(LOCK, LOCK_UN);
    close LOCK;

    # Tell the user what we've done
    print <<"EOT";
Copied the current quotes into $quotesfile
If there is a problem with this or you have to stop half way through or
something, this is where you will find the ones you were working on.

If you need to reprocess this file for some reason, run
$0 $quotesfile

Press Enter to continue...
EOT
    my $trash = <STDIN>;
}

open QUOTES, $quotesfile or die "Can't open $quotesfile: $!";

if (! -d "quotes") {
    mkdir "quotes" or die "Couldn't mkdir quotes: $!\n";
}

my %output;

my $choices = "\f";
my $i=0;
foreach my $sect (@sectkeys) {
    $i++;
    $output{$sect} = new FileHandle;
    $output{$sect} -> open(">>quotes/$sects{$sect}[1].tex")
        or die "Can't open quotes/$sects{$sect}[1].tex: $!";
    $output{$sect}->autoflush(1);
    $choices .= "\n" if $i % 4 == 1 and $i > 4;
    $choices .= "($sect) $sects{$sect}[0]   ";
}

$choices .= "\n\n";
my $quote='';

while (<QUOTES>) {
    next if /^\s*$/;
    $quote .= $_;
    if (/^\s*\% quote submitted by/) {
        my $sect = '';
	my $repeat = 0;
        until (exists $keyshash{$sect}) {
	    system("clear");
            print "$choices$quote\n\n";
	    print "Please try again.\n" if $repeat;
            print "Which section should this quote go into? ";
            $sect = <STDIN>;
            chomp $sect;
            if (exists $keyshash{$sect}) {
                $output{$sect}->print($quote, "\n");
                $quote = '';
            }
	    $repeat = 1;
        }
    }
}

close QUOTES;
foreach my $sect (@sectkeys) {
    $output{$sect}->close;
}

print "\n\n";
print "Thank you - quotes sorted into the files in the quotes direcotry.\n\n";

exit 0;
