<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" method="text" />
  <xsl:template match="/">% version=<xsl:value-of select="camper-entry/version" />

\begin{camper-entry}
\indexentry{<xsl:value-of select="/camper-entry/indexentry" />}
  \begin{top-part}
    <xsl:apply-templates select="/camper-entry/top-part/biographical-details" />
    \begin{photo}
      \includephoto<xsl:for-each
         select="/camper-entry/photo/options">[<xsl:value-of 
         select="."/>]</xsl:for-each>{<xsl:value-of
         select="/camper-entry/photo/filename"/>}
    \end{photo}
  \end{top-part}
  \begin{main-entry}
<xsl:value-of select="camper-entry/main-entry" />
  \end{main-entry}
\end{camper-entry}

    <xsl:for-each select="/comment">
\endinput

Comments:
<xsl:value-of select="/comment" />
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="/camper-entry/top-part/biographical-details">
    \begin{biographical-details}
    <xsl:if test="name != registeredname">
      %%% Registered name: <xsl:value-of select="registeredname"/>
    </xsl:if>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'name'"/>
      <xsl:with-param name="field" select="name"/>
    </xsl:call-template>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'address'"/>
      <xsl:with-param name="field" select="address"/>
    </xsl:call-template>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'phonenum'"/>
      <xsl:with-param name="field" select="phonenum"/>
    </xsl:call-template>

    <xsl:call-template name="multi_data">
      <xsl:with-param name="fieldname" select="'email'"/>
      <xsl:with-param name="field" select="emails"/>
    </xsl:call-template>

    <xsl:call-template name="multi_data">
      <xsl:with-param name="fieldname" select="'aim'"/>
      <xsl:with-param name="field" select="aims"/>
    </xsl:call-template>

    <xsl:call-template name="multi_data">
      <xsl:with-param name="fieldname" select="'icq'"/>
      <xsl:with-param name="field" select="icqs"/>
    </xsl:call-template>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'msn'"/>
      <xsl:with-param name="field" select="msn"/>
    </xsl:call-template>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'livejournal'"/>
      <xsl:with-param name="field" select="livejournal"/>
    </xsl:call-template>

    <xsl:for-each select="extrabiog">
      \begin{extrabiog}{<xsl:value-of select="@fieldname"/>}<xsl:value-of select="."/>\end{extrabiog}
    </xsl:for-each>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'birthday'"/>
      <xsl:with-param name="field" select="birthday"/>
    </xsl:call-template>

    <xsl:call-template name="single_data">
      <xsl:with-param name="fieldname" select="'school'"/>
      <xsl:with-param name="field" select="school"/>
    </xsl:call-template>

    \end{biographical-details}

  </xsl:template>

  <xsl:template name="single_data" >
    <xsl:param name="fieldname"/>
    <xsl:param name="field"/>
    <xsl:for-each select="$field">
      \begin{<xsl:value-of select="$fieldname"/>}<xsl:value-of
        select="$field"/>\end{<xsl:value-of select="$fieldname"/>}
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="multi_data" >
    <xsl:param name="fieldname"/>
    <xsl:param name="field"/>
    <xsl:for-each select="$field">
      \begin{<xsl:value-of select="$fieldname"/>s}
      <xsl:for-each select="./*[position()&lt;last()]">
        \<xsl:value-of select="$fieldname"/>{<xsl:value-of select="."/>}, 
      </xsl:for-each>
        \<xsl:value-of select="$fieldname"/>{<xsl:value-of select="./*[last()]" disable-output-escaping="yes"/>}
      \end{<xsl:value-of select="$fieldname"/>s}
    </xsl:for-each>
  </xsl:template>

</xsl:transform>
