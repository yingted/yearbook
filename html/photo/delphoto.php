<?php

// vim: se sw=4 sts=4:

require_once('photolib.php');
$finaldir = $ybhomedir.'/pics';

checkperms();
$loggedin or die("Not logged in.");
$isadmin or die("Not an admin.");

$person = $_GET['person'];

if (!array_key_exists($person, $people)) {
    echo "Person $person does not exist.";
    die();
}

$filename = $_GET['photo'];

$pdata = new Person($person);
$ybphoto = $pdata->get_yb_photo();
$locked = $pdata->is_locked();
(!$locked) or die("$person's entry is locked.");

$photo = $pdata->get_photo($filename);

$current = $photo->getAttribute('current');

// Delete everything
foreach($photo->getElementsByTagName('edit') as $edit) {
    $efilename = $edit->textContent;
    unlink($uploaddir.'/'.$efilename.'.jpg');
    unlink($uploaddir.'/thumb/'.$efilename.'.jpg');
    if ($ybphoto == $efilename) {
        @unlink($finaldir.'/'.$pdata->filename.'.jpg');
    }
}
unlink($uploaddir.'/'.$filename.'.jpg');
unlink($uploaddir.'/thumb/'.$filename.'.jpg');

// Delete photo
$photo->parentNode->removeChild($photo);
PersonDB::get()->dirty();

// Set a new current photo
if ($current) {
    $pdata->dirty_photos();
    $parray = $pdata->get_photos();
    if (count($parray)) {
    $parray[count($parray)-1]->setAttribute('current', 1);
    }
}

$pdata->reset_categories();


header("Location: viewperson.php?person={$person}");

?>
