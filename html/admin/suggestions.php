<?php
require_once('../config.php');
require_once('lib/quote.php');
$loggedin = require_priv('auth');
?>
<html>
<head>
<title>Comments admin</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php

echo "<h1>Yearbook comments list</h1>";
print `cat ../../pages/comments.txt | sed -e "s/---Submitted by \(.*\)/ <b>--\\1<\/b><br \/>/g"`;

echo "<h1>Entry comments list</h1>";
print `for x in \$(grep -l "<comment>" ../../pages/entries/*);
	do cat \$x | tr "\n" " " | sed -e "s/.*<comment>\(.*\)<\/comment>.*/\\1/g";
	echo \$x | sed -e "s/.*entries\/\(.*\).xml/ <b>--\\1<\/b><br\/>/g" | tr _ " ";
	done`;

echo "<h1>Log</h1>";
echo '<textarea cols="80" rows="10">';
print `tac ../../pages/log.txt | head -n 100`;
echo '</textarea>';
?>
</body>
</html>
