#! /usr/bin/perl -w
# Change camper name script.
# 
# This will change a camper's name in the campers.lst or campers.csv file
# (see the setting of $CSVfiles below), and update the corresponding XML
# and LaTeX files, the name of the photograph file, and also update
# people.php and superlatives.tex (if it exists).
# 
# This is useful because we usually have some campers whose official name
# turns out to be different from their common name.

# It is an interactive script, and asks for the original and new first name
# and last name.

use strict;
use File::Copy;

# Are we using CSV files campers.csv and staff.csv?
my $CSVfiles = 0;

my $ybhome = '/home/oem/yb';
chdir "$ybhome/pages" or die "Couldn't chdir $ybhome/pages: $!\n";

umask 002;  # files must be group writeable

my %baks = ();

print "Original first name of camper? ";
my $oldfirst = <STDIN>;
print "Original last name of camper? ";
my $oldlast = <STDIN>;
print "New first name of camper? ";
my $newfirst = <STDIN>;
print "New last name of camper? ";
my $newlast = <STDIN>;
chomp($oldfirst, $oldlast, $newfirst, $newlast);

my $oldfilename = "${oldfirst}_$oldlast";
$oldfilename =~ s/\s/_/g;
# Get rid of some problematic characters
$oldfilename =~ s/'|@|&|"/_/g;

my $filename = "${newfirst}_$newlast";
$filename =~ s/\s/_/g;
# Get rid of some problematic characters
$filename =~ s/'|@|&|"/_/g;

my $date = `date`;
chomp $date;

my (@campers, @staff);
my $found = '';

if ($CSVfiles) {
    my ($campers_csv, $staff_csv);

    open CAMPERS, "campers.csv"
	or die "Can't open pages/campers.csv for reading: $!\n";
    while (<CAMPERS>) {
	if (/^"(.*)","(.*)"\s*$/) {
	    if ($1 eq $oldfirst and $2 eq $oldlast) {
		push @campers, [$newfirst, $newlast];
		$found = 'campers';
		s/^"$oldfirst","$oldlast"/"$newfirst","$newlast"/;
	    } elsif ($1 eq $newfirst and $2 eq $newlast) {
		die "A camper called $newfirst $newlast already exists!\n";
	    } else {
		push @campers, [$1, $2];
	    }
	}
	$campers_csv .= $_;
    }
    close CAMPERS or warn "Problem reading campers.csv: $!\n";
    open STAFF, "staff.csv"
	or die "Can't open pages/staff.csv for reading: $!\n";
    while (<STAFF>) {
	next if /^\#/;
	if (/^"(.*)","(.*)"\s*$/) {
	    if ($1 eq $oldfirst and $2 eq $oldlast) {
		push @staff, [$newfirst, $newlast];
		$found = 'staff';
		s/^"$oldfirst","$oldlast"/"$newfirst","$newlast"/;
	    } elsif ($1 eq $newfirst and $2 eq $newlast) {
		die "A staff member called $newfirst $newlast already exists!\n";
	    } else {
		push @staff, [$1, $2];
	    }
	}
	$staff_csv .= $_;
    }
    close STAFF  or warn "Problem reading pages/staff.csv: $!\n";

    if ($found eq 'campers') {
	copy("campers.csv", "campers.csv.bak");
	$baks{"pages/campers.csv"} = "pages/campers.csv.bak";

	if (open CAMPERS, ">campers.csv") {
	    print CAMPERS $campers_csv;
	    close CAMPERS or warn "Problem writing to pages/campers.csv: $!\n";
	} else {
	    warn "Can't open pages/campers.csv for writing: $!\n";
	}
    } elsif ($found eq 'staff') {
	copy("staff.csv", "staff.csv.bak");
	$baks{"pages/staff.csv"} = "pages/staff.csv.bak";

	if (open STAFF, ">staff.csv") {
	    print STAFF $staff_csv;
	    close STAFF or warn "Problem writing to pages/staff.csv: $!\n";
	} else {
	    warn "Can't open pages/staff.csv for writing: $!\n";
	}
    }
} else {
    my ($campers_lst, $staff_lst);
    copy("campers.lst", "campers.lst.bak");
    copy("staff.lst", "staff.lst.bak");
    $baks{"pages/campers.lst"} = "pages/campers.lst.bak";
    $baks{"pages/staff.lst"} = "pages/staff.lst.bak";

    open CAMPERS, "campers.lst"
	or die "Can't open pages/campers.lst for reading: $!\n";
    while (<CAMPERS>) {
	chomp;
	if (/^([^\t]*)\t(.*?)\s*$/) {
	    if ($1 eq $oldfirst and $2 eq $oldlast) {
		push @campers, [$newfirst, $newlast];
		$found = 'campers';
		s/^$oldfirst\t$oldlast/$newfirst\t$newlast/;
	    } else {
		push @campers, [$1, $2];
	    }
	}
	$campers_lst .= "$_\n";
    }
    close CAMPERS or warn "Problem reading pages/campers.lst: $!\n";;
    open STAFF, "staff.lst" or die "Can't open pages/staff.lst: $!\n";
    while (<STAFF>) {
	chomp;
	if (/^([^\t]*)\t(.*?)\s*$/) {
	    if ($1 eq $oldfirst and $2 eq $oldlast) {
		push @staff, [$newfirst, $newlast];
		$found = 'staff';
		s/^$oldfirst\t$oldlast/$newfirst\t$newlast/;
	    } else {
		push @staff, [$1, $2];
	    }
	}
	$staff_lst .= "$_\n";
    }
    close STAFF or warn "Problem reading pages/staff.lst: $!\n";

    if ($found eq 'campers') {
	copy("campers.lst", "campers.lst.bak");
	$baks{"pages/campers.lst"} = "pages/campers.lst.bak";

	if (open CAMPERS, ">campers.lst") {
	    print CAMPERS $campers_lst;
	    close CAMPERS or warn "Problem writing to pages/campers.lst: $!\n";
	} else {
	    warn "Can't open pages/campers.lst for writing: $!\n";
	}
    } elsif ($found eq 'staff') {
	copy("staff.lst", "staff.lst.bak");
	$baks{"pages/staff.lst"} = "pages/staff.lst.bak";

	if (open STAFF, ">staff.lst") {
	    print STAFF $staff_lst;
	    close STAFF or warn "Problem writing to pages/staff.lst: $!\n";
	} else {
	    warn "Can't open pages/staff.lst for writing: $!\n";
	}
    }
}

die "Couldn't find camper/staff $oldfirst $oldlast in list!\n"
    unless $found;

foreach my $pic (glob("../pics/${oldfirst}_${oldlast}.*")) {
    next if $pic =~ /\.bak$/;
    my $newpic = $pic;
    $newpic =~ s%^\.\./pics/${oldfirst}_${oldlast}\.%../pics/${newfirst}_${newlast}.%;
    copy ($pic, "$pic.bak");
    move ($pic, $newpic);
    $pic =~ s%^\.\./%%;
    $newpic =~ s%^\.\./%%;
    $baks{$newpic} = "$pic.bak";
}

# Here are three files we will be modifying: we recreate people.php,
# we update campers.tex or staff.tex, and we modify superlatives.tex
# if it exists.
my $people_php = <<"EOT";
<?PHP
# Autogenerated by init: $date
unset(\$people);
EOT

my ($campers_tex, $staff_tex);
my $status = 'campers';

# Read the XML template
my $xmltemplate;
open XML, "template.xml" or die "Couldn't open XML template: $!\n";
{
    local $/;
    $xmltemplate = <XML>;
}
close XML;

# Start with campers.  Sort by first name.
@campers = sort { $$a[0] cmp $$b[0] } @campers;
@staff = sort { $$a[0] cmp $$b[0] } @staff;
foreach my $person (@campers, ['STAFF', 'STAFF'], @staff) {
    my $firstname = $$person[0];
    my $lastname = $$person[1];
    my $filename = "${firstname}_$lastname";
    $filename =~ s/\s/_/g;
    # Get rid of some problematic characters
    $filename =~ s/'|@|&|"/_/g;

    if ($firstname eq 'STAFF') {
	$people_php .= "\n";
	next;
    }

    if (! -e "entries/$filename.xml") {
	my $xml = $xmltemplate;
	$xml =~ s/\@\@FIRSTNAME\@\@/$firstname/g;
	$xml =~ s/\@\@LASTNAME\@\@/$lastname/g;
	$xml =~ s/\@\@PICTURE\@\@/$filename/g;
	if (open XMLOUT, "> entries/$filename.xml") {
	    print XMLOUT $xml;
	    close XMLOUT;
	    system("$ybhome/utils/ybxml2tex $filename");
	} else {
	    warn "Couldn't open XML output file pages/entries/$filename.xml: $!\n";
	}
    }

    $people_php .= "\$people[\"$filename\"]=\"$firstname $lastname\";\n";

    if ($status eq 'campers') {
	$campers_tex .= "\\input entries/$filename\n";
    } else {
	$staff_tex .= "\\input entries/$filename\n";
    }
}

$people_php .= "?>\n";

copy("../html/people.php", "../html/people.php.bak");
$baks{"html/people.php"} = "html/people.php.bak";
if (open PHP, ">../html/people.php") {
    print PHP $people_php;
    close PHP or warn "Problems closing html/people.php: $!\n";
} else {
    warn "Problem opening html/people.php for writing: $!\n";
}

if (-f "superlatives.tex") {
    if (open SUP, "superlatives.tex") {
	copy("superlatives.tex", "superlatives.tex.bak");
	$baks{"pages/superlatives.tex"} = "pages/superlatives.tex.bak";
	local $/;
	my $sup = <SUP>;
	close SUP or warn "Problems reading pages/superlatives.tex, trying to write anyway: $!\n";
	$sup =~ s/^\\item\[$oldfirst $oldlast\]/\\item[$newfirst $newlast]/;
	if (open SUP, ">superlatives.tex") {
	    print SUP $sup;
	    close SUP or warn "Problems writing pages/superlatives.tex: $!\n";
	} else {
	    warn "Problem opening pages/superlatives.tex for writing: $!\n";
	}
    } else {
	warn "Problem opening pages/superlatives.tex for reading: $!\n";
    }
} else {
    warn "pages/superlatives.tex does not exist or is not readable, not creating\n";
}

if (-f "../html/photo/photo_data.xml") {
    if (open PHOTODATA, "../html/photo/photo_data.xml") {
	copy("../html/photo/photo_data.xml", "../html/photo/photo_data.xml.bak");
	$baks{"html/photo/photo_data.xml"} = "html/photo/photo_data.xml.bak";
	local $/;
	my $photodata = <PHOTODATA>;
	close PHOTODATA or warn "Problems reading html/photo/photo_data.xml, trying to write anyway: $!\n";
	$photodata =~ s/<person>$oldfilename<\/person>/<person>$filename<\/person>/g;
	if (open PHOTODATA, ">../html/photo/photo_data.xml") {
	    print PHOTODATA $photodata;
	    close PHOTODATA or warn "Problems writing html/photo/photo_data.xml $!\n";
	} else {
	    warn "Problem opening html/photo/photo_data.xml for writing: $!\n";
	}
    } else {
	warn "Problem opening html/photo/photo_data.xml for reading: $!\n";
    }
} else {
    warn "html/photo/photo_data.xml does not exist or is not readable, not creating\n";
}

if (-f "../html/photo/photo_dump.xml") {
    if (open PHOTODUMP, "../html/photo/photo_dump.xml") {
	copy("../html/photo/photo_dump.xml", "../html/photo/photo_dump.xml.bak");
	$baks{"html/photo/photo_dump.xml"} = "html/photo/photo_dump.xml.bak";
	local $/;
	my $photodump = <PHOTODUMP>;
	close PHOTODUMP or warn "Problems reading html/photo/photo_dump.xml, trying to write anyway: $!\n";
	$photodump =~ s/xml:id=\"$oldfilename\"/xml:id=\"$filename\"/g;
	if (open PHOTODUMP, ">../html/photo/photo_dump.xml") {
	    print PHOTODUMP $photodump;
	    close PHOTODUMP or warn "Problems writing html/photo/photo_dump.xml $!\n";
	} else {
	    warn "Problem opening html/photo/photo_dump.xml for writing: $!\n";
	}
    } else {
	warn "Problem opening html/photo/photo_dump.xml for reading: $!\n";
    }
} else {
    warn "html/photo/photo_dump.xml does not exist or is not readable, not creating\n";
}


if ($found eq 'campers') {
    if (open CTEX, ">campers.tex") {
	copy("campers.tex", "campers.tex.bak");
	$baks{"pages/campers.tex"} = "pages/campers.tex.bak";
	print CTEX $campers_tex;
	close CTEX or warn "Problems closing pages/campers.tex: $!\n";
    } else {
	warn "Problem opening pages/campers.tex for writing: $!\n";
    }
} else {
    if (open STEX, ">staff.tex") {
	copy("staff.tex", "staff.tex.bak");
	$baks{"pages/staff.tex"} = "pages/staff.tex.bak";
	print STEX $staff_tex;
	close STEX or warn "Problems writing pages/staff.tex: $!\n";
    } else {
	warn "Problem opening pages/staff.tex for writing: $!\n";
    }
}

foreach my $ext (qw(xml tex)) {
    if (-f "entries/${oldfirst}_${oldlast}.$ext") {
	if (open ENTRY, "entries/${oldfirst}_${oldlast}.$ext") {
	    move("entries/${oldfirst}_${oldlast}.$ext",
		 "entries/${oldfirst}_${oldlast}.$ext.bak");
	    $baks{"pages/entries/${newfirst}_${newlast}.$ext"} = "pages/entries/${oldfirst}_${oldlast}.$ext.bak";
	    local $/;
	    my $entry = <ENTRY>;
	    close $entry or warn "Problems reading pages/entries/${oldfirst}_${oldlast}.$ext, trying to write anyway: $!\n";
	    if ($ext eq 'xml') {
		$entry =~ s%<indexentry>$oldlast, $oldfirst</indexentry>%<indexentry>$newlast, $newfirst</indexentry>%;
		$entry =~ s%<filename>${oldfirst}_${oldlast}</filename>%<filename>${newfirst}_${newlast}</filename>%;
		$entry =~ s%<registeredname>$oldfirst $oldlast</registeredname>%<registeredname>$newfirst $newlast</registeredname>%;
		$entry =~ s%<name>$oldfirst $oldlast</name>%<name>$newfirst $newlast</name>%;
	    } else {
		$entry =~ s%\\indexentry\{$oldlast, $oldfirst\}%\\indexentry{$newlast, $newfirst}%;
		$entry =~ s%\\begin\{name\}$oldfirst $oldlast\\end\{name\}%\\begin{name}$newfirst $newlast\\end{name}%;
		$entry =~ s%\\includephoto\{${oldfirst}_${oldlast}\}%\\includephoto\{${newfirst}_${newlast}\}%;
	    }
	    if (open ENTRY, ">entries/${newfirst}_${newlast}.$ext") {
		print ENTRY $entry;
		close ENTRY or warn "Problems writing pages/entries/${newfirst}_${newlast}.$ext: $!\n";
	    } else {
		warn "Couldn't open pages/entries/${newfirst}_${newlast}.$ext for writing: $!\n";
	    }
	} else {
	    warn "Couldn't open pages/entries/${oldfirst}_${oldlast}.$ext for reading: $!\n";
	}
    } else {
	warn "File pages/entries/${oldfirst}_${oldlast}.$ext not found!\n";
    }
}

sub END {
    print "Renamed $oldfirst $oldlast as $newfirst $newlast.\n";
    print "Changed or created the following files:\n";
    foreach my $new (sort { $a cmp $b } keys %baks) {
	print "  $new (old copy saved as $baks{$new})\n";
    }
}

exit 0;
