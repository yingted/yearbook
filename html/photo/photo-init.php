<?

// vim: se sw=4 sts=4:
require_once('photolib.php');

$pdb = PersonDB::get();
$cat = Categories::get();

foreach ($people as $id => $name) {
    if ($pdb->get_data($id)) {
    } else {
	$node = $pdb->dom->createElement('name');
	$node->setAttribute('xml:id', $id);

	$pnode = $pdb->dom->createElement('picture-status');
	$cnode = $pdb->dom->createElement('categories');

	$pnode->setAttribute('locked', 0);
	$pnode->setAttribute('requestnew', 0);

	addbr($node);
	$node->appendChild($pnode);
	addbr($node);
	$node->appendChild($cnode);
	addbr($node);

	$pdb->dom->documentElement->appendChild($node);
	addbr($pdb->dom->documentElement);

	$pdb->dirty();

	$cat->add_person_to('needphoto', $id);
    }
}

?>
