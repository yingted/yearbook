<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:transform version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >
    <xsl:output omit-xml-declaration="yes" method="html" indent="yes" />

    <xsl:template match='/' name='process-all'>
        <xsl:param name='reqstatus' />
        <xsl:param name='category' />
        <h2>Items</h2>
	<p><a href='?action=edit&amp;id=0' class='really-strong'><strong>Submit New</strong></a></p>
        <table class="item-table recs-table">
        <thead>
            <tr>
                <th>Submitter<br />Status<br />Category<br /></th>
                <th>Recommendation</th>
            </tr>
        </thead><tbody>
        <xsl:for-each select='/*/categories/item'>
            <xsl:sort select='@name' />
            <xsl:variable name='name' select='string(@name)' />
	    <xsl:for-each select="/*/items/*[category=$name]">
	        <xsl:call-template name='process-item' />
	    </xsl:for-each>
        </xsl:for-each>
        </tbody>
        </table>
	<p><a href='?action=edit&amp;id=0' class='really-strong'><strong>Submit New</strong></a></p>

        <h2>Categories</h2>
        <ul class='cats'>
            <xsl:call-template name='process-cats' />
        </ul>
	<p><strong><a href='?action=cat-edit'>Submit New Category</a></strong></p>

    </xsl:template>

    <xsl:template name='process-cats'>
        <xsl:for-each select='/*/categories/item'>
            <xsl:sort select="@name"/>
            <xsl:variable name="name" select="@name" />
            <li class='cat-lvl'>
                (<xsl:value-of select="$name" />)
                <a href='?action=cat-edit&amp;name={$name}'>
                    <xsl:value-of select='.' />
                </a></li>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name='process-item'>
        <tr>
            <td>
                <xsl:value-of select="submitter" /><br />
                Status: <xsl:value-of select="status" /><br />
                <xsl:variable name="cat" select="category" />
                Cat: <a href='?action=cat-edit&amp;name={$cat}'>
                    <xsl:value-of select='/*/categories/item[@name=string($cat)]' /></a><br />
                <xsl:variable name="idnum" select="@idnum" />
                <a href="?action=edit&amp;id={$idnum}">Edit</a>
            </td>
	    <xsl:variable name="isgone" select="status" />
            <xsl:choose>
            <xsl:when test="string($isgone)='duplicate' or string($isgone)='deleted'">
                <td bgcolor="#444444">
                 
                <dl class="item rec">
                    <xsl:if test="string-length(title) != 0">
                        <dt class="title">Title</dt>
                        <dd><cite><del><xsl:value-of select="title" /></del></cite></dd>
                    </xsl:if>
                    <xsl:if test="string-length(author) != 0">
                        <dt class="author">Author</dt>
                        <dd><del><xsl:value-of select="author" /></del></dd>
                    </xsl:if>
                    <xsl:if test="string-length(comments) != 0">
                        <dt class="comments">Comments</dt>
                        <dd><del><xsl:value-of select="comments" /></del></dd>
                    </xsl:if>
                </dl>
		</td>
            </xsl:when>
            <xsl:when test="string($isgone)='ready'">
                <td bgcolor="#00CCFF">

                <dl class="item rec">
                    <xsl:if test="string-length(title) != 0">
                        <dt class="title">Title</dt>
                        <dd><cite><xsl:value-of select="title" /></cite></dd>
                    </xsl:if>
                    <xsl:if test="string-length(author) != 0">
                        <dt class="author">Author</dt>
                        <dd><xsl:value-of select="author" /></dd>
                    </xsl:if>
                    <xsl:if test="string-length(comments) != 0">
                        <dt class="comments">Comments</dt>
                        <dd><xsl:value-of select="comments" /></dd>
                    </xsl:if>
                </dl>
		</td>
            </xsl:when>
            <xsl:otherwise>
                <td>

                <dl class="item rec">
                    <xsl:if test="string-length(title) != 0">
                        <dt class="title">Title</dt>
                        <dd><cite><xsl:value-of select="title" /></cite></dd>
                    </xsl:if>
                    <xsl:if test="string-length(author) != 0">
                        <dt class="author">Author</dt>
                        <dd><xsl:value-of select="author" /></dd>
                    </xsl:if>
                    <xsl:if test="string-length(comments) != 0">
                        <dt class="comments">Comments</dt>
                        <dd><xsl:value-of select="comments" /></dd>
                    </xsl:if>
                </dl>
		</td>
            </xsl:otherwise>
            </xsl:choose>
        </tr>
    </xsl:template>
</xsl:transform>
