#!/usr/bin/python
# Script to move tab-deliminated AoPS screennames into individual .xml files
# This program takes the screennames on stdin
# 2005-08-01 Alex Dehnert

from string import split
from string import strip
from string import replace
import string
import sys
import os

#ybentries = '%%YBROOT%%/pages/entries/'
ybentries = '/home/yearbook/yearbook/pages/entries/'

while 1:
	line = sys.stdin.readline()	 # read line-by-line
	if not line: break
	
	stuff = split(line, "\t", 1)
	
	name = stuff[0]
	sn   = stuff[1]
	name = string.strip(name)
	sn = string.strip(sn)
	xmlfile = ybentries + "/" + string.replace(name, ' ', '_') + '.xml'
	thefile = open(xmlfile, 'r')
	thetext = string.replace(thefile.read(), '</name>',
		"</name>\n      <extrabiog fieldname=\"aops\">" + sn + "</extrabiog>")
	thefile.close()
	thefile = open(xmlfile, 'w')
	thefile.write(thetext)
	thefile.close()
