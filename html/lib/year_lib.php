<?PHP
require_once(dirname(__FILE__) . '/../config.php');
require_once('yearparse.php');

if(session_id() == '') session_start();

/* This function returns a standard XHTML header */
function start_html($title)
{
  $heading = <<<EOT
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>$title</title>
EOT;
  return $heading;
}

/* Run this function over your input; fight against
   ridiculous magic_quotes feature. */
function input_escape_slashes($str)
{
  if (!get_magic_quotes_gpc())
    $str = addslashes($str);
  return $str;
}
/* Run this function over your input; fight against
   ridiculous magic_quotes feature. */
function input_unescape_slashes($str)
{
  if (get_magic_quotes_gpc())
    $str = stripslashes($str);
  return $str;
}

/* Replaces all whitespace with a single space character,
   also stripping leading and trailing whitespace */
function clean_space($str)
{
  $str = preg_replace('/\s+/', ' ', $str);
  $str = preg_replace('/^\s*/', '', $str);
  $str = preg_replace('/\s*$/', '', $str);
  return $str;
}

/* Remove un-needed chars from a filename.
   Prevents things like ../ from being used.
*/
function clean_filename($filename)
{
  return preg_replace('/[^a-zA-Z_\-]/','',$filename);
}

/* Quit with an error message. 
 */
function error($message)
{
  $message = preg_replace("/\n/","</p><p>",$message);
  $message = "<p>$message</p>";
  $message .= "<p>Please tell a yearbook staff member. Be sure to write down ";
  $message .= "or remember the message, or we won't be able to help you!</p>";
  print("\n\n<hr /><h3>Error (Something went wrong!):</h3>$message</body></html>\n");
  exit();
}

function xml_sanitize($newstr)
{
  $newstr = str_replace("&", "&amp;", $newstr);
  $newstr = str_replace("<", "&lt;", $newstr);
  $newstr = str_replace(">", "&gt;", $newstr);
  $newstr = preg_replace("\n?\r\n?", "\n", $newstr);
  return $newstr;
}

function tab_item($desc,$name,$value)
{
  if (isset($value)) { $val = $value; } else { $val = ''; }
  print <<<EOT
    <tr>
    <td width="180">
    $desc
    </td>
    <td align="left">
    <input size="70" type="text" name="$name" value="$val">
    </td>
    </tr>

EOT;
}

function tab_extra_item($keyname,$valname,$key,$value)
{
  if (isset($key))   { $k   = $key;   } else { $k   = ''; }
  if (isset($value)) { $val = $value; } else { $val = ''; }
  print <<<EOT
    <tr>
    <td width="180">
    <input size="20" type="text" name="$keyname" value="$k">
    </td>
    <td align="left">
    <input size="70" type="text" name="$valname" value="$val">
    </td>
    </tr>

EOT;
}

function l_tab_item($desc,$name,$value)
{
  if (isset($value)) { $val = $value; } else { $val = ''; }
  print <<<EOT
    <tr>
    <td>
    $desc
    </td>
    <td align="left">
    <textarea cols="60" rows="3" name="$name">$val</textarea>
    </td>
    </tr>

EOT;
}

class Entry
{
  var $filename;
  var $data;

  function Entry($filename)
  {
    $this->filename=$filename;
  }

  function get_field($field)
  {
    if (array_key_exists($field, $this->data)) {
      return $this->data[$field];
    } else {
      return False;
    }
  }

  function set_field($field, $value)
  {
    $this->data[$field] = $value;
  }

  function unset_field($field)
  {
    unset($this->data[$field]);
  }

  function load()
  {
    global $pagesprefix;
    $fileloc="$pagesprefix/entries/$this->filename.xml";
    $this->data = read_entry($fileloc);
    if ($this->data === False) {
      error("<b>There was a problem reading your yearbook entry!</b>\n<i>(Error message: xml parse failed)</i>\nIt is unclear why this has happened, so please find a yearbook techie and ask for help.");
    }
    /* is this needed?  there was an issue in the past.... */
    /* preg_replace("/\n+/", "\n", $this->text); */
  }

  function write_reg($key)
  {
    if (array_key_exists($key, $this->data)) {
      $val = xml_sanitize($this->data[$key]);
      $ret = "<$key>$val</$key>";
    } else {
      $ret = False;
    }
    return $ret;
  }

  function write_multi($key)
  {
    if (array_key_exists($key . 's', $this->data)) {
      $ret = "      <{$key}s>\n";
      foreach ($this->data[$key . 's'] as $v) {
	$val = xml_sanitize($v);
	$ret .= "        <$key>$val</$key>\n";
      }
      $ret .= "      </{$key}s>\n";
    } else {
      $ret = False;
    }
    return $ret;
  }

  function write_extrabiog()
  {
    if (array_key_exists('extrabiog', $this->data)) {
      $ret = '';
      foreach ($this->data['extrabiog'] as $k => $v) {
	$field = xml_sanitize($k);
	$val = xml_sanitize($v);
        $ret .= "      <extrabiog fieldname=\"$field\">$val</extrabiog>\n";
      }
    } else {
      $ret = False;
    }
    return $ret;
  }

  function write_photo()
  {
    $ret = "  <photo>\n";
    foreach ($this->data['photo'] as $k => $v) {
      $val = xml_sanitize($v);
      $ret .= "    <$k>$val</$k>\n";
    }
    $ret .= "  </photo>\n";
    
    return $ret;
  }

  function write_picture_status()
  {
    if (isset($this->data['picture-status'])) {
      $ret = '  <picture-status locked="';
      $ret .= $this->data['picture-status']['locked'];
      $ret .= '" requestnew="';
      $ret .= $this->data['picture-status']['requestnew'];
      $ret .= "\">\n";

      for ($i=0;$i<count($this->data['picture-status']['pictures']);$i++) {
	$pic = $this->data['picture-status']['pictures'][$i];
	$ret .= '    <picture filename="';
	$ret .= xml_sanitize($pic['filename']);
	$ret .= '" current="';
	if ($this->data['picture-status']['current'] == $i)
	  $ret .= 1;
	else
	  $ret .= 0;
	$ret .= "\">\n";
	foreach ($pic['edits'] as $edit) {
	  $ret .= '      <edit status="';
	  $ret .= intval($edit['status']);
	  $ret .= '" editor="';
	  $ret .= xml_sanitize($edit['editor']);
	  $ret .= '">';
	  $ret .= xml_sanitize($edit['filename']);
	  $ret .= "</edit>\n";
	}
	if (isset($pic['assign-to'])) {
	  $ret .= '      <assign time="';
	  $ret .= $pic['assign-at'];
	  $ret .= '">';
	  $ret .= $pic['assign-to'];
	  $ret .= "</assign>\n";
	}
	$ret .= "    </picture>\n";
      }

      if (isset($this->data['picture-status']['pic-complaint'])) {
	$ret .= '    <pic-complaint>';
	$ret .= xml_sanitize($this->data['picture-status']['pic-complaint']);
	$ret .= "</pic-complaint>\n";
      }
      $ret .= "  </picture-status>\n";
    } else {
      $ret = "\n"; // to be safe for now
    }

    return $ret;
  }

  function write()
  {
    global $pagesprefix;
    global $utilprefix;

    umask(002);  /* files must be group-writable */
    $new_file = <<<EOT
<?xml version="1.0"?>
<!DOCTYPE camper-entry SYSTEM "yearbook-entry.dtd">
<camper-entry>

EOT;

    $new_file .= "  " . $this->write_reg('version') . "\n";
    $new_file .= "  " . $this->write_reg('indexentry') . "\n";
    $new_file .= $this->write_photo();
    $new_file .= <<<EOT
  <top-part>
    <biographical-details>

EOT;

    # print_r($this->data);
    foreach (array('registeredname', 'name', 'address', 'phonenum', 'msn')
	     as $k) {
      $str = $this->write_reg($k);
      if ($str != False) {
	$new_file .= "      $str\n";
      }
    }

    foreach (array('email', 'aim', 'icq') as $k) {
      $str = $this->write_multi($k);
      if ($str != False) {
	$new_file .= $str;
      }
    }

    $str = $this->write_extrabiog();
    if ($str != False) {
      $new_file .= "$str";
    }

    foreach (array('livejournal', 'birthday', 'school') as $k) {
      $str = $this->write_reg($k);
      if ($str != False) {
	$new_file .= "      $str\n";
      }
    }

    $new_file .= <<<EOT
    </biographical-details>
  </top-part>

EOT;

    $new_file .= "  <main-entry>\n";
    $new_file .= xml_sanitize($this->data['main-entry']);
    $new_file .= "\n  </main-entry>\n";

    //$new_file .= $this->write_picture_status();

    $str = $this->write_reg('comment');
    if ($str != False) {
      $new_file .= "  $str\n";
    }

    $new_file .= "</camper-entry>\n";

    if (!file_exists("$pagesprefix/entries/$this->filename.xml"))
      error("The file for your yearbook entry doesn't exist?!?!?\nStrange... you were able to edit it. Find a yearbook staff member!");
    $fd=fopen("$pagesprefix/entries/$this->filename.xml","wb");
    if (!$fd)
      error("Could not save the yearbook entry! Find a yearbook staff member.");
    fputs($fd,$new_file);
    fclose($fd);

    // SVN.
    $fn_sani=escapeshellcmd($this->filename);
    exec("$utilprefix/svn");

    // Make LaTeX file
    putenv("PATH=/usr/bin:/bin");
    $output=shell_exec("make -C $pagesprefix/entries $fn_sani.tex");

    // Do other stuff here.

    // Update log file.
    $logmsg=date("Y/m/d H:i:s")." Updated $this->filename.xml.\n";
    if ($this->data['oldversion'] !== $this->data['version'])
      $logmsg .= "  This entry changed version from {$this->data['oldversion']} to {$this->data['version']}\n";
    if ($this->data['oldversion'] === "final")
      $logmsg .= "  WARNING: this was an edit of a final version!\n";
    $fd = fopen("$pagesprefix/log.txt","ab+");
    fputs($fd,$logmsg);
    fclose($fd);

    return $output;
  }

}

function check_login($usertype)
{
  global $passwords;
  if ($_SESSION['id'] == $usertype)
    return true;
  if (isset($_REQUEST['pass']) && $_REQUEST['pass'] == $passwords[$usertype])
    return true;
  else
    return false;
}

function get_user()
{
  if ($_SESSION['id'])
    return $_SESSION['id'];
  else
    return false;
}

function require_login($usertype, $dir_prefix = '')
{
  if (check_login($usertype))
    return true;
  else
    error("You don't seem to be logged in, but you need to be to use this part of the yearbook software.\n<a href=\"${dir_prefix}index.php\">Go to index</a>");
}

function require_priv($app, $action = '', $user = false)
{
  if (!user_can($app, $action, $user)) {
    error("You don't seem to have enough privileges to use this part of the yearbook software.\n<a href='index.php'>Go to index</a>");
  }
  else
    return true;
}

function user_can($app, $action = '', $user = false)
{
  $privs = array(
	     'staff' => array(
	       'quote' => array(
		 'list' => true,
		 'edit' => true,
		 'regen' => true,
	       ),
	       'rec' => array(
		 'list' => true,
		 'edit' => true,
		 'regen' => true,
	       ),
	       'galley' => array(
                 'list' => true,
		 'edit' => true,
		 'regen' => true,
	       ),
	       'photo' => array(
                 'edit' => true,
		 'admin' => true,
	       ),
	       'auth' => array(
		 'long' => true,
	       )
	     ),
	     'quotes' => array(
	       'quote' => array(
		 'list' => true,
		 'edit' => true,
		 'regen' => true,
	       ),
	       'auth' => array(
		 'long' => true,
	       )
	     ),
	     'mcstaff' => array(
	       'rec' => array(
		  'list' => true,
		  'edit' => true,
		  'regen' => true,
	       ),
	       'auth' => array(
		 'long' => true,
	       )
	     ),
	     'photoadmin' => array(
	       'photo' => array(
		 'edit' => true,
		 'admin' => true,
	       ),
	       'auth' => array(
		 'long' => true,
	       )
	     ),
	     'photoeditor' => array(
	       'photo' => array(
		 'edit' => true,
		 'admin' => false,
	       ),
	       'auth' => array(
		 'long' => true,
	       )
	     ),
	   );
  if ($user==false)
    $user = get_user();
  if ($action == '')
    return isset($privs[$user][$app]);
  if (isset($privs[$user][$app][$action]))
    return $privs[$user][$app][$action];
  else
    return false;
}

function update_galley($name = 'all')
{
  global $pagesprefix;
  if($name == 'all') {
    $target = 'galleys';
  } else {
    $target = "galley-$name.pdf";
    @unlink("$pagesprefix/$target");
  }
  $cmd = "PATH=\$PATH:/usr/bin\ncd $pagesprefix/ && make $target";
  $ret = $cmd;
  $ret .= "<pre>\n";
  ob_start();
  system($cmd . " 2>&1", $retval);
  $ret .= ob_get_contents();
  ob_end_clean();
  $ret .= "</pre>\n";
  return array($ret, $retval);
}
?>
