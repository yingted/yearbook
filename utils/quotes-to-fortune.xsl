<?xml version="1.0" encoding="ISO-8859-1"?>


<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" />
    <xsl:strip-space elements="*"/>

    <xsl:template match='/'>
	<xsl:for-each select="/quotes/quote">
	    <xsl:if test="./status=&quot;ready&quot;">
		<xsl:call-template name='process-quote' />
	    </xsl:if>
	</xsl:for-each><xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match='quote' name='process-quote'>
<xsl:for-each select='line'><xsl:text>
</xsl:text><xsl:value-of select='person' /><!-- 
    --><xsl:if test="string-length(context) != 0"> [<xsl:value-of select="context" />]</xsl:if>: <!--
    --><xsl:value-of select='text' />
</xsl:for-each>
%</xsl:template>
</xsl:transform>
