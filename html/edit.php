<?PHP
require_once('config.php');
require_once('year_lib.php');

session_start();

// Initialise variables from form
foreach($_REQUEST as $varname => $varval)
{
  $_REQUEST[$varname]=($_REQUEST[$varname]);
}

$heading = start_html("Edit your entry");
$heading .= <<<EOT
    <link rel="stylesheet" type="text/css" href="style.css">
    <!--http://christianp.github.io/writemaths/-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <script type="text/javascript" src="rangy-core.js"></script>
    <script type="text/javascript" src="rangy-position.js"></script>
    <script type="text/javascript" src="textinputs_jquery.js"></script>
    <script type="text/javascript" src="jquery.caretposition.js"></script>
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
       tex2jax: {inlineMath: [["$","$"]]},
       displayAlign: "center",
       displayIndent: "0.1em"
      });
    </script>
    <script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML.js"></script>
    <script type="text/javascript" src="writemaths.js"></script>
    <script language="javascript">
       $(document).ready(function() {
          $('.wm').writemaths();
       });
    </script>
  </head>
  <body>
  <h1>Edit your entry</h1>
EOT;

$filename=$_REQUEST['file'];

// Have they chosen a file to edit?
if (strlen($filename) == 0) {
  if (isset($_SESSION['id']) and $_SESSION['id'] !== 'staff') {
    // kick 'em out
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time()-42000, '/');
    }

    // Finally, destroy the session.
    session_destroy();
  }

  print <<<EOT
$heading

<p>
You need to select an entry to edit!  Please
<a href="index.php">go back</a> and try again.
</p>
  </body>
</html>

EOT;
  exit(0);
}

$filename=clean_filename($filename);
$fn_sani=escapeshellcmd($filename);

if (isset($_REQUEST['pass'])) {
  $pass = $_REQUEST['pass'];
  if ($pass === $passwords['late']) {
    $_SESSION['id'] = $fn_sani;
    $_SESSION['idtype'] = 'late';
  } elseif ($pass === $passwords['unlock']) {
    $_SESSION['id'] = $fn_sani;
    $_SESSION['idtype'] = 'unlock';
  } elseif ($pass === $passwords['staff']) {
    $_SESSION['id'] = 'staff';
    $_SESSION['idtype'] = 'staff';
  }
}

if ($usepasswords) {
  if (! isset($_SESSION['id'])) {
    print <<<EOT
$heading

<p>
Sorry, your password has expired or was not recognised.  You may
<a href="index.php">go back</a> to try again.
</p>
</body>
</html>

EOT;
    exit(0);
  }
  elseif ($_SESSION['id'] !== 'staff' and $_SESSION['id'] !== $fn_sani) {
    // And they were doing naughties, so we'll kick them out...
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time()-42000, '/');
    }
    
    // Finally, destroy the session.
    session_destroy();

    print <<<EOT
<p>
Sorry, your password was only valid for editing the original entry.  You may
<a href="index.php">go back</a> to try again.
</p>
</body>
</html>

EOT;

    exit(0);
  }
}

// Now we can load up the entry
$entry=new Entry("$fn_sani");
$entry->load();

// What are we trying to do?  We're just editing unless we've been given
// one special option...
if ($_REQUEST['action'] === 'Final version') {
  $version = $entry->get_field('version');
  if ($version !== "final" && $version !== "draft") {
    // Ouch!
    if ($_SESSION['id'] !== 'staff') {
      // clear their session
      $_SESSION = array();
      if (isset($_COOKIE[session_name()])) {
	setcookie(session_name(), '', time()-42000, '/');
      }
    }
    print <<<EOT
$heading

<p>Something really weird happened: you were trying to mark your entry as
final, but it's been marked for hand-editing only.  Please speak to a
member of the Yearbook staff!</p>

<p>Click <a href="index.php">here</a> to return to the Yearbook homepage.</p>

</body>
</html>

EOT;
//'
    exit(0);
  } else {
    $entry->set_field('version','final');
    $entry->set_field('oldversion',$version);
    $entry->write();

    if ($_SESSION['id'] !== 'staff') {
      // clear their session; don't want them to go on editing their entry now
      $_SESSION = array();
      if (isset($_COOKIE[session_name()])) {
	setcookie(session_name(), '', time()-42000, '/');
      }
    }

    print <<<EOT
$heading

<p>Thank you!  Your entry has now been marked as in its final form.
It is still subject to editing by the staff, though.</p>

<p>Click <a href="index.php">here</a> to return to the Yearbook homepage.</p>

</body>
</html>

EOT;
    exit(0);
  }
}

// So just plain normal editing now...
foreach (array('version', 'registeredname', 'name', 'address', 'phonenum',
	       'msn', 'livejournal', 'emails', 'aims', 'icqs', 'extrabiog',
	       'birthday', 'school', 'comment') as $k) {
  $$k = $entry->get_field($k);
}
$main_entry = $entry->get_field('main-entry');
$photo_arr = $entry->get_field('photo');
$photo = $photo_arr['filename'];
$photoopt = $photo_arr['options'];

print $heading;
print "<h2>$registeredname</h2>\n";



if ($version === "final" &&
    (! isset($_SESSION['id']) or
     ($_SESSION['idtype'] !== 'staff' and $_SESSID['idtype'] !== 'unlock'))) {
  /* Just in case it's been deleted */
  system("$utilprefix/makedraftpdf $filename",$status);

  print <<<EOT
<form action="unlock.php"
      enctype="multipart/form-data"
      method="post">
<p>You can preview your final entry 
<a href="drafts/$filename.pdf" target="_blank">here</a>.  (The final
version of your page is still subject to the editors checking it,
though.)</p>

<p>If you wish to make changes at this point, please contact the
yearbook staff to request that your page be unlocked.</p>

<p>Unlock password: <input type="password" name="pass" /></p>
<br />
<p>
<input type="hidden" name="_charset_" />
<input type="hidden" name="filename" value="$filename" />
<input type="submit" value="Unlock file" />
</p>

</form>
</body>
</html>

EOT;
  exit(0);
}
elseif ($version !== "final" && $version !== "draft") {
  /* so this has been marked as hand-editing only... */ 
  /* Just in case it's been deleted */
  system("$utilprefix/makedraftpdf $filename",$status);

  print <<<EOT
<h1>View your submitted entry</h1>
<p>Your entry is marked for hand editing only.</p>

<p>You can possibly preview your entry 
<a href="drafts/$filename.pdf" target="_blank">here</a>.
(The final version of your page is still subject to the editors
checking it, though.)</p>

<p>If you wish to make changes, please contact the yearbook staff to
discuss this.</p>

<form action="index.php" method="post">
<input type="submit" value="Continue">

</form>
</body>
</html>

EOT;
  exit(0);
}

// Only remaining cases: final with password given, or draft
?>
<form action="update.php"
      enctype="multipart/form-data"
      method="post">
<table style="border:1px solid black;background:#99ccff;width:500px;">
<tr>
  <td>
<?PHP
# exec("$utilprefix/makethumb $photo");
if (file_exists("$htmlprefix/thumbpics/${photo}_thumb.jpg"))
  print "<img src=\"thumbpics/${photo}_thumb.jpg\"";
else
  print "<img src=\"thumbpics/nopicture_thumb.jpg\"";
print " style=\"float:left;border:5px solid transparent;\">\n";
?>
  If your picture does not show up, it doesn't necessarily mean that
  it wasn't taken; it just means that we haven't edited it yet.
<br />
  If there is something wrong with your picture, <em>tell us</em>.
</td>
</tr>
</table>
<br />
<table width="700">
<?PHP  #'
tab_item("Name","name",$name);
if ($_SESSION['id'] === 'staff') {
  tab_item("Photo options","photoopt",$photoopt);
  print <<<EOT
<tr>
  <td width="180">
  </td>
  <td align="left">
    (This is for additional options to be given to \includegraphics for
    the entry's photograph.)
  </td>
</tr>

EOT;
#'
}
// l_tab_item("<em>Most likely to...<em>","superlative",$superlative);
l_tab_item("Address","address",$address);
tab_item("Phone","phonenum",$phonenum);
print <<<EOT
<tr>
  <td colspan="2">
    <i>You will be subscribed to the Mathcamp alumni mailing lists with
      the first email address given.</i>
  </td>
</tr>

EOT;
tab_item("Email Address 1","email1",$emails[0]);
tab_item("Email Address 2","email2",$emails[1]);
tab_item("Email Address 3","email3",$emails[2]);
tab_item("AIM Name 1","aim1",$aims[0]);
tab_item("AIM Name 2","aim2",$aims[1]);
tab_item("AIM Name 3","aim3",$aims[2]);
tab_item("ICQ Number 1","icq1",$icqs[0]);
tab_item("ICQ Number 2","icq2",$icqs[1]);
tab_item("ICQ Number 3","icq3",$icqs[2]);
tab_item("MSN Name","msn",$msn);
tab_item("Livejournal name","livejournal",$livejournal);

print <<<EOT
<tr>
  <td colspan="2">
    <p><i>You can add up to four other pieces of information
      here (e.g., AoPS username).  Enclose emails or websites in \url{...} for
      correct formatting.</i></p>
  </td>
</tr>

EOT;
$i = 1;
if(is_array($extrabiog))
{
    foreach ($extrabiog as $k => $v) {
        tab_extra_item("biogname$i", "biogval$i", $k, $v);
        $i++;
    }
}
for ( ; $i <= 4; $i++) {
  tab_extra_item("biogname$i", "biogval$i", '', '');
}
tab_item("Birthday","birthday",$birthday);
tab_item("School and Grade this Fall","school",$school);

?>
<tr>
  <td colspan="2">
<br />
<p>
  Now for your actual entry. Your entry should be written in LaTeX. Visit <a href="latexHelp.html" target="_blank">this page</a> (opens in a new window) for some formatting tips, <b>even if you already know how to use LaTeX</b>.
</p>


<br />
<textarea cols="80" rows="25" name="text" class="wm">
<?PHP print $main_entry ?>
</textarea>
  </td>
</tr>
<tr>
  <td colspan="2">
Any comments for the editors about your page?
<br />
<textarea cols="80" rows="5" name="comment">
<?PHP print $comment ?>
</textarea>
  </td>
</tr>
<tr>
  <td colspan="2">
    <input type="submit" value="Save changes">
    <input type="reset" value="Revert to old values">
  </td>
</tr>
</table>
<input type="hidden" name="filename" value="<?PHP print $filename ?>">

</form>
</body>
</html>
