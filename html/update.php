<?PHP
require('config.php');
require('year_lib.php');

session_start();

// Initialise variables from form
/*
foreach($_REQUEST as $varname => $varval)
{
  $_REQUEST[$varname]=input_escape_slashes($_REQUEST[$varname]);
}*/

$filename=$_REQUEST['filename'];

$filename=clean_filename($filename);
$fn_sani=escapeshellcmd($filename);
$entry=new Entry("$fn_sani");
$entry->filename=$filename;
$entry->load();
$version = $entry->get_field('version');
$entry->set_field('oldversion',$version);
$photo_arr = $entry->get_field('photo');

if ($version !== 'draft' && $version !== 'final') {
  // oops, we shouldn't be here at all
  print start_html("Problem saving changes...");
  print <<<EOT
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
EOT;
  error("Oops, we're trying to modify an entry marked as manual modifications only!");
}

if ($version === 'final' || ($usepasswords && $version === 'draft')) {
  if ($_SESSION['id'] !== 'staff' and $_SESSION['id'] !== $fn_sani) {
    print start_html("Problem saving changes...");
    print <<<EOT
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <h1>Mathcamp Yearbook: Problem saving changes...</h1>
    <p>Sorry, there seems to be a problem editing your page; either:</p>
    <ul><li>your password is incorrect, or</li>
        <li>the system has now been locked for changes, or</li>
        <li>you have already marked your entry as final, or</li>
        <li>the system is now locked for changes.</li>
    </ul>

    <p>You could return to the <a href="index.php">Yearbook Homepage</a> or
     speak to a Yearbook Staff Member.</p>

    <p>Your main text was:</p>

    <textarea rows="24" cols="80">
{$_REQUEST['text']}
    </textarea>

  </body>
</html>

EOT;
    exit(0);
  }
}
// Can now output the normal heading...

print start_html("Saving changes...");
?>

<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>Mathcamp Yearbook: Saving changes...</h1>
<form action="edit.php"
      enctype="multipart/form-data"
      method="post">

<?PHP
$problems = True;

foreach (array('name', 'address', 'phonenum', 'msn', 'livejournal',
	       'birthday', 'school', 'comment') as $k) {
  if (strlen($_REQUEST[$k])) { $entry->set_field($k, $_REQUEST[$k]); }
  else { $entry->unset_field($k); }
}

foreach (array ('email', 'aim', 'icq') as $k) {
  $$k = array();
  for ($i=1; $i<=3; $i++)
    if (strlen($_REQUEST["$k$i"]) > 0)
      ${$k}[] = $_REQUEST["$k$i"];
  if (count($$k) > 0)
    $entry->set_field("${k}s", $$k);
  else
    $entry->unset_field("${k}s");
}

$extra = array();
for ($i=1; $i<=4; $i++)
  if (strlen($_REQUEST["biogname$i"]) > 0)
    $extra[$_REQUEST["biogname$i"]] = $_REQUEST["biogval$i"];
if (count($extra) > 0)
  $entry->set_field('extrabiog', $extra);
else
  $entry->unset_field('extrabiog');

if ($_SESSION['id'] === 'staff') {
  if (strlen($_REQUEST['photoopt']) > 0) {
    $photo_arr['options'] = $_REQUEST['photoopt'];
  } else {
    unset($photo_arr['options']);
  }
  $entry->set_field('photo', $photo_arr);
}

$entry->set_field('main-entry', input_unescape_slashes($_REQUEST['text']));
$write_output=$entry->write();

/* print <<EOT
<p><b><font color="red">If we tell you "Well, I finished modifying your
entry, but there are problems", YOU HAVE AN ERROR! Go fix it.
When your entry is completely finished, preview and finalize your entry.
MUST BE DONE BY SIGN-IN TODAY (Thursday, July 29th)!!
</font></b><p>
<br />
<br />
EOT;
*/

print <<<EOT
<p>Saved file. Here's the output from LaTeX (and a few other programs):</p>

<pre><textarea rows="10" cols="80" readonly="readonly">
$write_output

EOT;
#'

system("$utilprefix/makedraftpdf $filename",$status);
$output = xml_sanitize(file_get_contents("$htmlprefix/errors/$fn_sani.out"));
print $output;
print "</textarea></pre>\n";

if ($status==0) {
  print <<<EOT
<p>There don't seem to be any errors! Yay!</p>
<img src="Pic_Thumbs_Up.jpg" alt="Yay!" />
<br />
EOT;
#'

  $pages=file_get_contents("$htmlprefix/errors/$fn_sani.pages");
  $pages=rtrim($pages);
  if ($pages!='1') {
    print "<p>Well, except that your entry is too many pages... you'll
need to bring it down to one page; right now, you're using
$pages pages.</p>\n";
    if ($pages>2) {
      print "<p>That's right, an entire $pages pages! The suggestions
below might help a bit, but $pages pages is so much that you should
probably just remove some of the content in your entry.</p>\n";
    }
?>
<p>If you don't, the yearbook staff will randomly remove things
from your entry to bring it down to one page!</p>
<p>
Anyway, I'll still let you preview your entry. When you're done, go back
and edit your entry.</p>
<p>
A few hints for reducing the size of your entry:</p>
<ul>
  <li>You can make your text smaller. Try adding one of these commands to
      the beginning of your entry (these go from largest to smallest):
    <ul>
      <li><tt>\small</tt></li>
      <li><tt>\tiny</tt></li>
      <li><tt>\scriptsize</tt></li>
    </ul></li>
  <li>You can also try multiple columns. You can do this by putting the command
      <tt>\begin{multicols}{2}</tt> at the beginning of your entry, and 
      <tt>\end{multicols}</tt> at the end.</li>
</ul>
<p>If you need help with any of this, just find a yearbook staff member!</p>

<?PHP #'
    // There were problems; this had better not be marked 'final'
    if ($version === 'final') {
      $version = 'draft';
      $entry->set_field('version',$version);
      $entry->write();
    }
  } else {
    unlink("$htmlprefix/errors/$fn_sani.pages");
    // Absolutely no problems at all!
    $problems = False;
  }

  if (strstr($entry->text,'"') || strstr($entry->ext,"'")) {
?>
<p>
A note: I've noticed that you've used a quotation mark character in your entry.
LaTeX (the typesetting language we use) distinguishes between opening quotation
marks and closing ones.</p>
<p>
Instead of typing something like <tt>"quote"</tt>, you should type
<tt>``quote''</tt>, starting with two backticks (on the top-left of the
keyboard) and ending with two apostrophes.</p>
<p>
If you need any help, find someone who knows LaTeX! :)
</p>
<?PHP
  }

  if ($version === "draft") {
    print <<<EOT
<p>You can preview your entry 
<a href="drafts/$filename.pdf" target="_blank">here</a>.</p>

EOT;
    if (! $problems) {
      print <<<EOT
<br />
<p>If you have finished editing your entry and wish to mark is
as your final version, please click the "Final version" button.  Once you
have done this, only the yearbook staff will be able to modify your
entry.</p>

<p>Alternatively, you can go back and edit your entry some
more should you wish to.</p>

EOT;
    }
  } else {
    print <<<EOT
<p>You can preview your final entry 
<a href="drafts/$filename.pdf" target="_blank">here</a>.  (The final
version of your page is still subject to the editors checking it,
though.)
</p>

EOT;
  }
} else {
?>
<p><font color="red"><b>Well, I finished modifying your entry, but there
are problems.</b></font> You can
look at LaTeX's log by clicking
<a href="drafts/<?PHP echo $filename ?>.log" target="_blank">here</a>.
After that, you can go back and edit your entry some more.</p>
<p>
It's likely that your problem is listed in the box below. If you don't know
what any of this means, find someone who knows LaTeX!</p>
<pre><textarea cols=80 rows=5>
<?PHP #'
$pos = strpos($output, "!");
print substr($output, $pos);
# system("egrep -A 1 '^(!|l\.|Error:)' $pagesprefix/drafts/$fn_sani.log");
?>
</textarea></pre>
<?PHP
  // As there were problems; this had better not be marked 'final'
  if ($version === 'final') {
    $version = 'draft';
    $entry->set_field('version',$version);
    $entry->set_field('oldversion','final');
    $entry->write();
  }
}

  print <<<EOT
  <p>
  <input type="hidden" name="_charset_" />
  <input type="hidden" name="file" value="$filename" />
  <input type="submit" name="action" value="Edit page some more" />
EOT;

if ($version === 'draft' && ! $problems) {
    print "<input type=\"submit\" name=\"action\" value=\"Final version\" />\n";
}
print "</p></form>\n";

if ($_SESSION['id'] === 'staff') {
  print <<<EOT
<p><b>
  Remember to <a href="logout.php">logout</a> or exit your browser
  (IE, Safari, Mozilla, whatever) when you are done, as you have used
  the staff password!!
</b></p>

EOT;
}

?>

<br />
<p>You can also click <a href="index.php">here</a> to return to the Yearbook
homepage.</p>
</body>
</html>
