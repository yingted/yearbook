<?php

// vim: set sw=4 sts=4:

require_once('photolib.php');

checkperms();

$person = $_GET['person'];
$photo = $_GET['photo'];

if (!array_key_exists($person, $people)) {
    die("Person $person does not exist.");
}

$pdata = new Person($person);
$locked = $pdata->is_locked();

(!$locked) or die("Entry is locked");

//$pcur = $pdata->get_photo_current();
$pcur = $pdata->get_photo($photo);
$claim = photo_get_assign($pcur) or die("No claim on this photo\n");

$editor = $claim->textContent;
$time = intval($claim->getAttribute('time'));
$time_s = date("Y-m-d g:i a", $time);
?>
<?php
if($loggedin)
{
?>
<html>
<head>
<title>Edit Photo Claim for <? echo $people[$person] ?></title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="photostyle.css">
</head>
<body>
<p><a href="index.php">Back to photos</a></p>
<h1>Edit Claim for <? echo $people[$person] ?></h1>
<?
    $editor_escaped = htmlspecialchars($editor);
    echo "<p><i>Claimed by $editor_escaped at $time_s</i></p>";
    echo "<img src=\"img/thumb/{$pcur->getAttribute('filename')}.jpg\" />\n";

    echo "<p><a href=\"cancelclaim.php?person={$person}&amp;photo={$photo}\">Cancel claim</a></p>";
?>
<b>Upload Edit</b>
<form method="post" action="uploadedit.php" enctype="multipart/form-data">
<input type="hidden" name="MAX_FILE_SIZE" value="1100000" />
<input type="hidden" name="person" value="<? echo htmlspecialchars($person, ENT_QUOTES); ?>" />
<input type="hidden" name="photo" value="<? echo htmlspecialchars($photo, ENT_QUOTES); ?>" />
<input type="hidden" name="editor" value="<? echo htmlspecialchars($editor, ENT_QUOTES); ?>" />
<input type="file" name="photo" />
<input type="submit" name="sub" value="Upload" />
</form>
</body>
</html>
<?
} else {
    draw_stupid_login_form();
}
?>
