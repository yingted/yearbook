<?php

require_once('photolib.php');

// vim: set sw=4 sts=4:

checkperms();
$loggedin or die("Not logged in.");
$isadmin or die("Need permissions.");

$person = $_POST['person'];
$photo = $_POST['photo'];
$editname = $_POST['editname'];
$status = intval($_POST['status']);

if ($status < -1 || $status > 1)
    die();

isset($_POST['sub']) or die();

if (!array_key_exists($person, $people)) {
    die("Person $person does not exist.");
}

$pdata = new Person($person);
$locked = $pdata->is_locked();

(!$locked) or die("Entry is locked");

$pcur = $pdata->get_photo($photo) or die("Photo does not exist.");
$pedit = photo_get_edit($pcur,$editname) or die("Edit does not exist.");

$oldstatus = $pedit->getAttribute('status');
if ($oldstatus != $status) {
    $pedit->setAttribute('status', $status);
    PersonDB::get()->dirty();
    $pdata->reset_categories();

    if ($status == 1) {
        $pdata->push_yb_photo($editname);
    }
}

header("Location: viewperson.php?person={$person}");

?>
