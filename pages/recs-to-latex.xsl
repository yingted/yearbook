<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" />

    <xsl:template match='/'>
        <xsl:for-each select='/*/categories/item'>
            <xsl:sort select='@name' />
            <xsl:variable name='name' select='string(@name)' />
            <xsl:choose>
<!--BEGIN MODIFICATION BY HERMAN-->
<!--NOTE: THIS IS A REALLY BAD HACK-->
	      <xsl:when test='contains(@name, "%")'>
	        <xsl:if test='count(/*/items/*[category=$name and status="ready"])>0'>
                  \subsubsection{<xsl:value-of select='.' />}
                </xsl:if>
              </xsl:when>
	      <xsl:when test='contains(@name, "$")'>
	        <xsl:if test='count(/*/items/*[category=$name and status="staff_name"])>0'>
                  \subsection{<xsl:value-of select='.' />}
                </xsl:if>
              </xsl:when>
<!--END MODIFICATION BY HERMAN-->
              <xsl:when test='contains(@name, "/")'>
		<xsl:if test='count(/*/items/*[category=$name and status="ready"])>0'>
                  \subsection{<xsl:value-of select='.' />}
		</xsl:if>
	      </xsl:when>
              <xsl:otherwise>
                \section{<xsl:value-of select='.' />}
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test='count(/*/items/*[category=$name and status="ready"])>0'>
                <xsl:call-template name='process-cat'>
                    <xsl:with-param name='category'>
                        <xsl:value-of select='./@name' />
                    </xsl:with-param>
                    <xsl:with-param name='reqstatus'>ready</xsl:with-param>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
<!--
        \section{Submitted}
        <xsl:call-template name='process-cat'>
            <xsl:with-param name='reqstatus'>submit</xsl:with-param>
        </xsl:call-template>
-->
    </xsl:template>

    <xsl:template name='process-cat'>
        <xsl:param name='reqstatus' >bar</xsl:param>
        <xsl:param name='category' />
\begin{recommendations}
        <xsl:for-each select="/*/items/rec">
            <xsl:if test="(./category=$category or $category='')">
                <xsl:if test="$reqstatus='' or ./status=$reqstatus">
                    <xsl:call-template name='process-item' />
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
\end{recommendations}
    </xsl:template>

    <xsl:template match='quote' name='process-item'>
\recommend
    {<xsl:value-of select='author' />}
    {<xsl:value-of select='title' />}
    {<xsl:value-of select='comments' />}
% Submitter: <xsl:value-of select='person' />
% Status: <xsl:value-of select='status' />
% ID: <xsl:value-of select='@idnum' />
    </xsl:template>
</xsl:transform>
