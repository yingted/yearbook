<?PHP
require_once('config.php');
require_once('quote.php');
umask(002);

print start_html("Quotes");
?>

<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

Click <a href="index.php">here</a> to return to the yearbook main page.

<h1>Quotes</h1>
<h2>Instructions</h2>

<p>Please include first name and last initial (when ambiguous,
include the full last name)!</p>

<p>Please don't submit multiple quotes at once; the extra text boxes are for
quoting people who reply to the original comment.</p>

<p>If you don't feel like typing (or if too many people are involved in the
quote!), write down your quotes and put them in the box at the notice
board.</p>

<h2>The Quote</h2>
<?php
$editer = new Quote();
$id = intval($_REQUEST['id']);
$editer->do_edit_form($id);
?>

</body>
</html>
