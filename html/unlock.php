<?PHP
require_once('config.php');
require_once('year_lib.php');

session_start();

// Initialise variables from form
foreach($_REQUEST as $varname => $varval)
{
  $_REQUEST[$varname]=input_escape_slashes($_REQUEST[$varname]);
}

$heading = start_html("Unlocking account...");
$heading .= <<<EOT
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

EOT;

$filename=$_REQUEST['filename'];

$filename=clean_filename($filename);
$fn_sani=escapeshellcmd($filename);
$pass=$_REQUEST['pass'];

if ($_SESSION['id'] !== 'staff' && isset($_REQUEST['pass'])) {
  if ($pass === $passwords['unlock']) {
    $_SESSION['id'] = $fn_sani;
    $_SESSION['idtype'] = 'unlock';
  } elseif ($pass === $passwords['staff']) {
    $_SESSION['id'] = 'staff';
    $_SESSION['idtype'] = 'staff';
  }
}

if ($_SESSION['idtype'] !== 'staff' and $_SESSION['idtype'] !== 'unlock') {
  print <<<EOT
<h1>Mathcamp Yearbook: Unlock failed!</h1>
<p><b>Your password was not recognised.  Please try again.</b></p>
<p>You can still preview your final entry
<a href="drafts/$filename.pdf" target="_blank">here</a>.  (The final
version of your page is still subject to the editors checking it,
though.)</p>

<form action="unlock.php"
      enctype="multipart/form-data"
      method="post">
Unlock password: <input type="password" name="pass">
<input type="hidden" name="filename" value="$filename">
<input type="submit" value="Unlock file">
</form>

<br />
<p>Return to <a href="index.php">Yearbook homepage</a>.</p>
</body>
</html>

EOT;
  exit(0);
}

$entry=new Entry("$fn_sani");
$entry->load();
$entry->set_field('version','draft');
$entry->set_field('oldversion','draft');  /* spare the log file from a warning
                                             about editing a final version! */
$entry->write();
// Update log file.
$logmsg=date("Y/m/d H:i:s")." UNLOCKED $entry->filename.tex.\n";
$fd=fopen("$pagesprefix/log.txt","ab+");
fputs($fd,$logmsg);
fclose($fd);
?>

<h1>Mathcamp Yearbook: Unlock successful</h1>
<p>You can now click the button below to edit your entry.
</p>

<form action="edit.php"
      enctype="multipart/form-data"
      method="post">
  <input type="hidden" name="file" value="<?PHP echo $filename ?>">
  <input type="submit" name="action" value="Edit page">
</form>
</body>
</html>
